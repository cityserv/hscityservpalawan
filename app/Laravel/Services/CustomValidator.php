<?php 

namespace App\Laravel\Services;


use Illuminate\Validation\Validator;
use Auth, Hash, Str, Input;

use App\Laravel\Models\User;
use App\Laravel\Models\CRTrackerHeader;
use App\Laravel\Models\CRCategory;

class CustomValidator extends Validator {

    public function validateUniqueFb($attribute,$value,$parameters){
        $fb_id = Str::lower($value);
        $is_unique = User::whereRaw("LOWER(fb_id) = '{$fb_id}'")->whereIn('type',['user'])->first();

        return $is_unique ? FALSE : TRUE;
    }

    public function validateUniqueEmail($attribute,$value,$parameters){
        $email = Str::lower($value);
        $user_id = FALSE;
        if($parameters){
            $user_id = $parameters[0];
        }

        if($user_id){
            $is_unique = User::where('id','<>',$user_id)->whereRaw("LOWER(email) = '{$email}'")->whereIn('type',['user'])->first();
        }else{
            $is_unique = User::whereRaw("LOWER(email) = '{$email}'")->whereIn('type',['user'])->first();
        }

        return $is_unique ? FALSE : TRUE;
    }

    public function validateOldPassword($attribute,$value,$parameters){
        // $user = User::find(Input::get('user_id',0));
        if($parameters){
            $user_id = $parameters[0];
            $user = User::find($user_id);
            return Hash::check($value,$user->password);
        }

        return FALSE;
    }

    public function validatePasswordFormat($attribute,$value,$parameters){
    	return preg_match(("/^(?=.*)[A-Za-z\d][A-Za-z\d!@#$%^&*()_+]{2,25}$/"), $value);
    }

    public function validateValidUser($attribute,$value,$parameters){
    	$value = Str::lower($value);
    	$valid_users = ['user'];

    	return in_array($value, $valid_users);
    }

    public function validateValidGender($attribute,$value,$parameters){
    	$gender = Str::lower($value);
    	$valid_genders = ["male","female"];

    	return in_array($gender, $valid_genders);
    }

    public function validateValidDateRange($attribute,$value,$parameters){
        $from = Input::get($parameters[0]);
        $to = Input::get($parameters[1]);

        $bool = 1;

        if($from AND $to){
            $reference_date = date_create(Helper::date_db($from));
            $comparative_date = date_create(Helper::date_db($to));
            $date_difference = date_diff($reference_date,$comparative_date)->format("%R%a") + 0;
            $bool = ( $date_difference >= 0) ? TRUE : FALSE;
        }

        return $bool;
    }

    public function validateValidType($attribute,$value,$parameters){
        $type = Str::lower($value);
        $valid_types = ["mac"];
        return in_array($type, $valid_types);
    }

    public function validateValidSubType($attribute,$value,$parameters){
        $sub_type = Str::lower($value);
        $type = Str::lower(Input::get('type'));
        $valid_sub_types = ["mac" => ['medical','burial','scholarship']];
        
        if(array_key_exists($type, $valid_sub_types)){
            return in_array($sub_type, $valid_sub_types["{$type}"]);
        }else{
            return FALSE;
        }
    }

    public function validateValidRequestType($attribute,$value,$parameters){
        $request_type = $value;
        
        $valid_request_types = [
            'SIMPLE TRANSFER (TR)|assessor_simple',
            'REASSESSMENT DUE TO DISPUTE (DP)|assessor_simple',
            'ENTER TO IDLE (EI)|assessor_simple',
            'DROP TO IDLE (DI)|assessor_simple',
            'TAXABLE TO EXEMPT (TE)|assessor_simple',
            'EXEMPT TO TAXABLE (ET)|assessor_simple',
            'PHYSICAL CHANGE (PC)|assessor_simple',
            'DISCOVERY/NEW DECLARATION (DC)|assessor_with_taxmapping',
            'SUBDIVISION (SD)|assessor_with_taxmapping',
            'RECLASSIFICATION (RC)|assessor_with_taxmapping',
            'CONSOLIDATION (CS)|assessor_with_taxmapping',
            'REASSESSMENT DUE TO DISPUTE (DP) (RECTIFICATION OF PIN)|assessor_with_taxmapping',
            'CANCELLATION OF REAL PROPERTY (CA)|assessor_with_taxmapping',
        ];

        return in_array($request_type, $valid_request_types);
    }

    public function validateValidAppointmentSchedule($attribute,$value,$parameters){
        $allowed_max_request = 3;
        $category_code = Input::get('category',"social_service");

        $count_requests = CRTrackerHeader::where('category_code', $category_code)
                            ->where("appointment_schedule", "=", Helper::datetime_db($value))
                            ->count();

        return ($count_requests >= $allowed_max_request) ? FALSE : TRUE;
    }

} 