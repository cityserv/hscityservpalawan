<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class SurveyRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			
		];
	}

	public function messages(){
		return [
			'required' => "Field is required.",
		];
	}
}