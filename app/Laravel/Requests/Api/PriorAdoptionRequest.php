<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class PriorAdoptionRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			'req_address' => 'required',
			'lname' => 'required',
			'fname' => 'required',
			'middle_name' => 'required',
			'father_lname' => 'required',
			'father_fname' => 'required',
			'father_middle_name' => 'required',
			'mother_maiden_lname' => 'required',
			'mother_maiden_fname' => 'required',
			'mother_maiden_middle_name' => 'required',
			// 'contact' => 'required',
			'place_of_birth' => 'required',
			'date_of_birth' => 'required'
			// 'purpose' => 'required',
			// 'number_of_copies' => 'required'

		];
	}

	public function messages(){
		return [
			'required' => "Field is required."
		];
	}
}