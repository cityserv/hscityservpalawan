<?php 

namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class MarriageCertificateRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{	
		return [
			'req_address' => 'required',
			'husband_lname' => 'required',
			'husband_fname' => 'required',
			'husband_middle_name' => 'required',
			'wife_lname' => 'required',
			'wife_fname' => 'required',
			'wife_middle_name' => 'required',
			'contact' => 'required',
			'place_of_marriage' => 'required',
			'date_of_marriage' => 'required',
			'purpose' => 'required',
			'number_of_copies' => 'required'

		];
	}

	public function messages(){
		return [
			'required' => "Field is required."
		];
	}
}