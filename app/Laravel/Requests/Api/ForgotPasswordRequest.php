<?php namespace App\Laravel\Requests\Api;

use App\Laravel\Requests\ApiRequestManager;

class ForgotPasswordRequest extends ApiRequestManager {

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules(){

		return [
				'email'			=> "required|email",
			];
			

		
	}

	public function messages(){
		return [
			'required'				=> "Field is required.",
		];
	}

}
