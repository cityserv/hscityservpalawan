<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class UserRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'type' => "required",
			'fname' => "required",
			'lname' => "required",
			'email' => "required|unique:user,email,".$id,
			'contact_number' => "required",
			'password' => "required|confirmed",
			'birthdate' => "date",
			'file' => "image",
		];

		if($id AND !$this->get('password', FALSE))	unset($rules['password']);
		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
		];
	}
}