<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class EstablishmentRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'directory_id' => "required|exists:directory,id",
			'name' => "required",
			'status' => "required|in:draft,published",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'status.in' => "Invalid status.",
			'directory_id.exists' => "Invalid directory",
		];
	}
}