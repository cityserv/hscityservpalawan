<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class PageRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;

		$rules = [
			'title' => "required",
			'status' => "required|in:draft,published",
		];

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'status.in' => "Invalid status.",
		];
	}
}