<?php 

namespace App\Laravel\Requests\Backoffice;

use Session,Auth;
use App\Laravel\Requests\RequestManager;

class TrackerRequest extends RequestManager{

	public function rules(){

		$id = $this->segment(3)?:0;
		$rules = [
			// 'task' => "required|in:info_validation,assessment_evaluation,final_interview,requirement_submission",
			// 'status' => "required|in:done,rejected",
			'remarks' => "required",
			'from' => "required|date",
			'to' => "required|date|after:from",
			'release_amount' => "numeric",
		];


		if(Auth::user()->type != "super_user"){
			unset($rules['from']);
			unset($rules['to']);
		}

		if($this->get('is_last_process', FALSE)){
			$rules['release_amount'] = "required|numeric";
		}

		return $rules;
	}

	public function messages(){
		return [
			'required' => "This item is required.",
			'status.in' => "Invalid status.",
			'task.in' => "Invalid task.",
			'to.after' => "This item must be a date after the field 'From'."
		];
	}
}