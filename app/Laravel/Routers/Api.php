<?php

$router->group([

	/**
	*
	* Api routes main config
	*/
	'namespace' => "Api",
	'as' => "api.",
	// 'domain' => env('BACKOFFICE_URL',"backoffice.pepero.localhost.com"),
	'prefix' => "api",
	'middleware'=> "api"

	], function(){

	/**
	*
	* Summernote upload route
	*/
	$this->post('summernote-upload.{data_format?}',['as' => "summernote",'uses' => "SummernoteController@upload"]);

	/**
	*
	* Authentication route
	*/
	$this->group(['prefix' => "auth"],function(){
		$this->post('register.{data_format?}',['uses' => "AuthController@store"]);
		$this->post('login.{data_format?}',['uses'	=> "AuthController@authenticate"]);
		$this->post('fb-login.{data_format?}',['uses'	=> "AuthController@fb_login"]);
		$this->any('logout.{data_format}',['uses' => 'AuthController@destroy']);
		$this->any('remove-email.{data_format}',['uses' => 'AuthController@remove_email']);
		$this->any('forgot-password.{data_format}',['uses' => 'AuthController@forgot_password']);
		$this->any('reset-password.{data_format}',['uses' => 'AuthController@reset_password']);
		//$this->any('resend-activation.{data_format}',['uses' => 'AuthController@resend_activation']);
	});

	/**
	*
	* App Setting upload route
	*/
	$this->group(['prefix' => "app-setting"],function(){
		$this->post('all.{data_format?}',['uses' => "AppSettingController@index"]);
	});

	/**
	*
	* Queue
	*/
	$this->group(['prefix' => "queue"],function(){
		$this->any('all.{data_format?}',['uses' => "QueueController@index"]);

		$this->any('updated-data.{data_format?}',['uses' => "QueueController@get_last_data"]);
		$this->any('service-data.{data_format?}',['uses' => "QueueController@get_service_data"]);
		$this->any('on-queue.{data_format?}',['uses' => "QueueController@on_queue"]);
		$this->any('for-display.{data_format?}',['uses' => "QueueController@for_display"]);
		$this->post('create.{data_format?}',['uses' => "QueueController@store"]);
		$this->group(['middleware' => "api.valid-queue"], function() {
			$this->group(['middleware' => "api.valid-teller"], function() {
				$this->post('flash.{data_format}',['uses' => "QueueController@flash"]);
				$this->post('start.{data_format}',['uses' => "QueueController@start"]);
				$this->post('stop.{data_format}',['uses' => "QueueController@stop"]);
				$this->post('cancel.{data_format}',['uses' => "QueueController@cancel"]);
				$this->post('prioritize.{data_format}',['uses' => "QueueController@prioritize"]);
				$this->post('transfer.{data_format}',['uses' => "QueueController@transfer"]);
			});
		});
		$this->group(['middleware' => "api.valid-queue-for-display"], function() {
			$this->post('flash-for-display.{data_format}',['uses' => "QueueController@flash_for_display"]);
		});
	});


	$this->group(['middleware' => "api.auth"],function(){

		/**
		*
		* Article route
		*/
		$this->group(['prefix' => "article"],function(){
			$this->any('all.{data_format}',['uses' => 'ArticleController@index']);
			$this->any('recent.{data_format}',['uses' => 'ArticleController@recent']);
			$this->any('featured.{data_format}',['uses' => 'ArticleController@featured']);
			$this->group(['middleware' => "api.valid-article"],function(){
				$this->any('show.{data_format}',['uses' => 'ArticleController@show']);
			});
		});

		/**
		*
		* Announcement route
		*/
		$this->group(['prefix' => "announcement"],function(){
			$this->any('all.{data_format}',['uses' => 'AnnouncementController@index']);
			$this->any('recent.{data_format}',['uses' => 'AnnouncementController@recent']);
			$this->any('featured.{data_format}',['uses' => 'AnnouncementController@featured']);
			$this->group(['middleware' => "api.valid-announcement"],function(){
				$this->any('show.{data_format}',['uses' => 'AnnouncementController@show']);
			});
		});

		/**
		*
		* Poll route
		*/
		$this->group(['prefix' => "poll"],function(){
			$this->any('all.{data_format?}',['uses' => 'PollController@index']);
			$this->any('create.{data_format?}',['uses' => "PollController@store"]);
			$this->any('pending.{data_format?}',['uses' => 'PollController@pending']);
			$this->any('show.{data_format?}',['uses' => 'PollController@show']);
		});

		/**
		*
		* Events routes
		*/
		$this->group(['prefix' => "event"],function(){
			$this->any('all.{data_format?}',['uses' => "AppEventController@index"]);
			$this->any('show.{data_format?}',['uses' => "AppEventController@show"]);
			$this->any('by-date.{data_format?}',['uses' => "AppEventController@by_date"]);
		});

		/**
		*
		* Page route
		*/
		$this->group(['prefix' => "page"],function(){
			$this->any('all.{data_format}',['uses' => 'PageController@index']);
			$this->group(['middleware' => "api.valid-page"],function(){
				$this->any('show.{data_format}',['uses' => 'PageController@show']);
			});
		});

		/**
		*
		* Citizen Report route
		*/
		$this->group(['prefix' => "citizen-report"],function(){
			$this->any('all.{data_format?}',['uses' => "CitizenReportController@index"]);
			$this->any('types.{data_format?}',['uses' => "CitizenReportController@types"]);
			$this->group(['middleware' => "api.valid-report"],function(){
				$this->any('admin-show.{data_format}',['uses' => 'AdminCitizenReportController@show']);
				$this->any('show.{data_format}',['uses' => 'CitizenReportController@show']);
				$this->any('delete.{data_format?}',['uses' => "CitizenReportController@destroy"]);
			});
			$this->any('my-reports.{data_format?}',['uses' => "CitizenReportController@my_reports"]);
			$this->any('create.{data_format?}',['uses' => "CitizenReportController@store"]);
			$this->any('pending.{data_format?}',['uses' => "CitizenReportController@pending"]);
			$this->any('on-going.{data_format?}',['uses' => "CitizenReportController@on_going"]);
			$this->any('resolved.{data_format?}',['uses' => "CitizenReportController@resolved"]);
			$this->any('admin-pending.{data_format?}',['uses' => "AdminCitizenReportController@pending"]);
			$this->any('admin-on-going.{data_format?}',['uses' => "AdminCitizenReportController@on_going"]);
			$this->any('admin-resolved.{data_format?}',['uses' => "AdminCitizenReportController@resolved"]);
			$this->any('admin-update.{data_format?}',['uses' => "AdminCitizenReportController@update"]);
		});

		/**
		*
		* Death Certificate Request route
		*/
		$this->group(['prefix' => "death-certificate-request"],function(){
			$this->any('all.{data_format?}',['uses' => "DeathCertificateController@index"]);
			$this->any('types.{data_format?}',['uses' => "DeathCertificateController@types"]);
			$this->group(['middleware' => "api.valid-report"],function(){
				$this->any('show.{data_format}',['uses' => 'DeathCertificateController@show']);
				$this->any('delete.{data_format?}',['uses' => "DeathCertificateController@destroy"]);
			});
			$this->any('my-requests.{data_format?}',['uses' => "DeathCertificateController@my_reports"]);
			$this->any('create.{data_format?}',['uses' => "DeathCertificateController@store"]);
			$this->any('pending.{data_format?}',['uses' => "DeathCertificateController@pending"]);
			$this->any('approved.{data_format?}',['uses' => "DeathCertificateController@approved"]);
		});

		/**
		*
		* Marriage Certificate Request route
		*/
		$this->group(['prefix' => "marriage-certificate-request"],function(){
			$this->any('all.{data_format?}',['uses' => "MarriageCertificateController@index"]);
			$this->any('types.{data_format?}',['uses' => "MarriageCertificateController@types"]);
			$this->group(['middleware' => "api.valid-report"],function(){
				$this->any('show.{data_format}',['uses' => 'MarriageCertificateController@show']);
				$this->any('delete.{data_format?}',['uses' => "MarriageCertificateController@destroy"]);
			});
			$this->any('my-requests.{data_format?}',['uses' => "MarriageCertificateController@my_reports"]);
			$this->any('create.{data_format?}',['uses' => "MarriageCertificateController@store"]);
			$this->any('pending.{data_format?}',['uses' => "MarriageCertificateController@pending"]);
			$this->any('approved.{data_format?}',['uses' => "MarriageCertificateController@approved"]);
		});

		/**
		*
		* Prior to Adoption Request route
		*/
		$this->group(['prefix' => "prior-adoption-request"],function(){
			$this->any('all.{data_format?}',['uses' => "PriorAdoptionController@index"]);
			$this->any('types.{data_format?}',['uses' => "PriorAdoptionController@types"]);
			$this->group(['middleware' => "api.valid-report"],function(){
				$this->any('show.{data_format}',['uses' => 'PriorAdoptionController@show']);
				$this->any('delete.{data_format?}',['uses' => "PriorAdoptionController@destroy"]);
			});
			$this->any('my-requests.{data_format?}',['uses' => "PriorAdoptionController@my_reports"]);
			$this->any('create.{data_format?}',['uses' => "PriorAdoptionController@store"]);
			$this->any('pending.{data_format?}',['uses' => "PriorAdoptionController@pending"]);
			$this->any('approved.{data_format?}',['uses' => "PriorAdoptionController@approved"]);
		});

		/**
		*
		* After Adoption Request route
		*/
		$this->group(['prefix' => "after-adoption-request"],function(){
			$this->any('all.{data_format?}',['uses' => "AfterAdoptionController@index"]);
			$this->any('types.{data_format?}',['uses' => "AfterAdoptionController@types"]);
			$this->group(['middleware' => "api.valid-report"],function(){
				$this->any('show.{data_format}',['uses' => 'AfterAdoptionController@show']);
				$this->any('delete.{data_format?}',['uses' => "AfterAdoptionController@destroy"]);
			});
			$this->any('my-requests.{data_format?}',['uses' => "AfterAdoptionController@my_reports"]);
			$this->any('create.{data_format?}',['uses' => "AfterAdoptionController@store"]);
			$this->any('pending.{data_format?}',['uses' => "AfterAdoptionController@pending"]);
			$this->any('approved.{data_format?}',['uses' => "AfterAdoptionController@approved"]);
		});

		/**
		*
		* Directory route
		*/
		$this->group(['prefix' => "directory"],function(){
			$this->any('all.{data_format?}',['uses' => "DirectoryController@index"]);
			$this->group(['middleware' => "api.valid-directory"],function(){
				$this->any('show.{data_format}',['uses' => 'DirectoryController@show']);
			});
		});

		/**
		*
		* Service route
		*/
		$this->group(['prefix' => "service"],function(){
			$this->any('all.{data_format}',['uses' => 'ServiceController@index']);
			$this->group(['middleware' => "api.valid-service"],function(){
				$this->any('show.{data_format}',['uses' => 'ServiceController@show']);
			});
		});

		/**
		*
		* Subservice route
		*/
		$this->group(['prefix' => "subservice"],function(){
			$this->any('all.{data_format}',['uses' => 'SubServiceController@index']);
			$this->group(['middleware' => "api.valid-subservice"],function(){
				$this->any('show.{data_format}',['uses' => 'SubServiceController@show']);
			});
		});

		/**
		*
		* Establishment route
		*/
		$this->group(['prefix' => "establishment"],function(){
			$this->any('all.{data_format?}',['uses' => "EstablishmentController@index"]);
			$this->group(['middleware' => "api.valid-establishment"],function(){
				$this->any('show.{data_format}',['uses' => 'EstablishmentController@show']);
				$this->any('list.{data_format?}',['uses' => "EstablishmentController@establishment_list"]);
			});
		});

		/**
		*
		* Widget route
		*/
		$this->group(['prefix' => "widget"],function(){
			$this->any('all.{data_format?}',['uses' => "WidgetController@index"]);
			$this->group(['middleware' => "api.valid-widget"],function(){
				$this->any('show.{data_format}',['uses' => 'WidgetController@show']);
			});
		});

		/**
		*
		* User route
		*/
		$this->group(['prefix' => "user"],function(){
			$this->any('profile.{data_format}',['uses' => 'UserController@profile']);
			$this->group(['prefix' => "update"],function(){
				$this->post('profile.{data_format}',['uses' => 'UserController@update_profile']);
				$this->post('password.{data_format}',['uses' => 'UserController@update_password']);
				$this->post('avatar.{data_format}',['uses' => 'UserController@update_avatar']);
				$this->any('field.{data_format}',[ 'uses' => "UserController@update_field"]);
				$this->any('reg-id.{data_format}',[ 'uses' => "UserController@update_reg_id"]);
			});
		});

		/**
		*
		* Citizen Report route
		*/
		$this->group(['prefix' => "citizen-request"],function(){
			$this->any('all.{data_format?}',['uses' => "CitizenRequestController@index"]);
			$this->group(['middleware' => "api.valid-request"],function(){
				$this->any('show.{data_format}',['uses' => 'CitizenRequestController@show']);
			});
			$this->any('my-requests.{data_format?}',['uses' => "CitizenRequestController@my_requests"]);
			$this->post('create.{data_format?}',['uses' => "CitizenRequestController@store"]);
			$this->post('track.{data_format?}',['uses' => "CitizenRequestController@track"]);
			$this->post('create-v2.{data_format?}',['uses' => "CitizenRequestController@store_v2"]);
			$this->any('survey.{data_format?}',['uses' => "CitizenRequestController@survey"]);
			$this->any('delete-survey.{data_format?}',['uses' => "CitizenRequestController@truncate_survey"]);
		});

		/**
		*
		* Citizen Moment route
		*/
		$this->group(['prefix' => "moment"],function(){
			$this->any('all.{data_format?}',['uses' => "MomentController@index"]);
			$this->group(['middleware' => "api.valid-moment"],function(){
				$this->any('show.{data_format}',['uses' => 'MomentController@show']);
				$this->any('edit.{data_format}',['uses' => 'MomentController@update']);
				$this->any('admin-edit.{data_format}',['uses' => 'MomentController@admin_update']);
				$this->any('delete.{data_format}',['uses' => 'MomentController@destroy']);
			});
			$this->any('my-moments.{data_format?}',['uses' => "MomentController@my_moments"]);
			$this->any('create.{data_format?}',['uses' => "MomentController@store"]);
		});

		/**
		*
		* Weather routes
		*/
		$this->group(['prefix' => "weather"],function(){
			$this->any('forecast.{data_format?}',['uses' => "WeatherController@forecast"]);
		});

		/**
		*
		* Notification
		*/
		$this->group(['prefix' => "notification"],function(){
			$this->any('all.{data_format?}',['uses' => "UserLogController@index"]);
		});



	});


});
