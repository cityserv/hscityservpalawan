<?php 

namespace App\Laravel\Controllers\Api;

use Illuminate\Contracts\Auth\Guard;

use App\Laravel\Models\User;
use App\Laravel\Models\UserDevice;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\UserTransformer;

use App\Laravel\Requests\Api\UserRequest;
use App\Laravel\Requests\Api\FbInfoRequest;
use App\Laravel\Requests\Api\ForgotPasswordRequest;
use App\Laravel\Requests\Api\ResetPasswordRequest;

use Event;
use App\Laravel\Events\LogUserAction;
use App\Laravel\Events\EmailForgotPassword;
use App\Laravel\Events\EmailActivationCode;

use Helper, ImageUploader, Carbon, Session, Input, Str, Auth, URL, DB;
use Agent,Request;

// use App\Knowheretogo\Events\PushNotification;



class AuthController extends Controller{

	/*
	|--------------------------------------------------------------------------
	| Authenticator Controller
	|--------------------------------------------------------------------------
	|
	|
	*/

	protected $response;

	public function __construct(Guard $auth){
		$this->auth = $auth;
		$this->response = array(
			"msg" => "Bad Request.",
			"status" => FALSE,
			'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
	}

	private function _generate_username($str){
		$new_username = Helper::str_clean(Str::lower($str));
		$check_username = User::where('username', 'like', "%{$str}%")->withTrashed()->count();
		if($check_username) $new_username = $new_username.($check_username + 1);
		return $new_username;

	}

	/**
	 * Login
	 * @format return data format
	 * @return Response
	 */
	public function authenticate($format = ""){
		try{
			$username = Input::get('username');
			$password = Input::get('password');
			$remember_me = Input::get('remember_me');

			$field = filter_var($username, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';

			$this->response["msg"] = "Incorrect email or password.";
			$this->response['status_code'] = "AUTH_INVALID";
			$this->response_code = 422;
			
			if(Auth::attempt([$field => $username,'password' => $password],$remember_me)){

				if(!in_array(Auth::user()->type, ["user","admin"])){
					Auth::logout();
					goto callback;
				}

				if(Auth::user()->is_active == "no") {
					$this->response["msg"] = "Your account is not yet activated, check your email and use the link provided to use this account.";
					$this->response['status_code'] = "AUTH_INACTIVE";
					Auth::logout();
					goto callback;
				}

				$user = User::find(Auth::user()->id);
				$user->last_login = Carbon::now();
				$user->save();

				$device = UserDevice::where('user_id',$user->id)->where('device_id',Input::get('device_id'))->first();

				if(Input::has('device_id')){
					UserDevice::where('device_id',Input::get('device_id'))
								->where('user_id','<>',$user->id)
								->update(['is_login' => "0"]);
								
					if(!$device){
						$new_device = new UserDevice;
						$new_device->user_id = $user->id;
						$new_device->reg_id = Input::get('device_reg_id');
						$new_device->device_id = Input::get('device_id');
						$new_device->device_name = Input::get('device_name');
						$new_device->is_login = 1;
						$new_device->save();
					}else{
						$device->reg_id = Input::get('device_reg_id');
						$device->device_name = Input::get('device_name');
						$device->is_login = 1;
						$device->save();
					}
				}

				$this->response['msg'] = "Login Successfully.";
				$this->response['status_code'] = "AUTH_TRUE";
				$this->response['status'] = TRUE;
				$this->response_code = 200;
				$this->response['first_login'] = false;
				$this->response['data'] = $this->transformer->transform($this->auth->user(), new UserTransformer,'item');
			}else{

				$is_email = User::where('email',$username)->whereIn('type',['user','admin'])->first();
				if(!$is_email){
					$this->response["msg"] = "Email address does not belong to any account.";
					$this->response['status_code'] = "AUTH_INVALID";
					$this->response_code = 401;
				}
			}	

			callback:
			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

				default :
				$this->response['msg'] = "Invalid return data format.";
				$this->response['status_code'] = "INVALID_FORMAT";
				$this->response['status'] = FALSE;
				$this->response_code = 406;
				return response()->json($this->response,$this->response_code);
			}
			
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	/**
	 * Logout
	 * @format return data format
	 * @return Response
	 */
	public function destroy($format = ""){
		try{
			$user_id = Input::get('auth_id');
			$device_id = Input::get('device_id');
			
			$device = UserDevice::where('user_id',$user_id)
							->where('device_id',$device_id)
							->first();

			if($device){
				$device->is_login = 0;
				$device->save();

				$user = User::find($user_id);
				if($user){
					$user->last_activity = NULL;
					$user->save();
				}
			}
			
			$this->response['msg'] = "Successfully Logout";
			$this->response['status_code'] = "AUTH_LOGOUT";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
				return response()->json($this->response,$this->response_code);

				break;

				default :
				$this->response['msg'] = "Invalid return data format.";
				$this->response['status_code'] = "INVALID_FORMAT";
				$this->response['status'] = FALSE;
				$this->response_code = 406;
				return response()->json($this->response,$this->response_code);
			}


		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	/**
	 * Registration 
	 * @format return data format
	 * @return Response
	 */
	public function store($format = "",UserRequest $request){
		try{

			DB::beginTransaction();
			$new_user = new User;
			$new_user->fill($request->except(['email','birthdate']));

			if($request->has('fb_id')){
				$new_user->fb_id = $request->get('fb_id');
				$new_user->email = $request->get('fb_id')."@facebook.com";
				$new_user->password = bcrypt(uniqid());
			}else{
				// $new_user->password = bcrypt(uniqid());
				$new_user->password = bcrypt($request->get('password'));
				if($request->has("email")) $new_user->email = Str::lower($request->get('email'));
			}

			$birthdate = $request->get('birthdate');
			if($birthdate) {
				$new_user->birthdate = Helper::date_db($birthdate);
				$new_user->age = Carbon::parse($birthdate)->age;
			}

			$new_user->type = "user";
			$new_user->username = $this->_generate_username($request->get('fname'));
			$new_user->activation_code = Str::random(40);
			$new_user->is_active = "yes";
			
			// if($request->hasFile('file')) $new_user->fill(ImageUploader::upload($request, "uploads/users", "file"));

			if($new_user->save()){

				$new_user->last_login = Carbon::now();
				$new_user->save();

				if($request->has('device_id')){
					
					UserDevice::where('device_id',$request->get('device_id'))
								->where('user_id','<>',$new_user->id)
								->update(['is_login' => "0"]);

					$device = new UserDevice;
					$device->user_id = $new_user->id;
					$device->reg_id = $request->get('device_reg_id');
					$device->device_id = $request->get('device_id');
					$device->device_name = $request->get('device_name');
					$device->is_login = 1;
					$device->save();
				}

				// $notification_data = new EmailActivationCode(['user_id' => $new_user->id]);
				// Event::fire('activation-code', $notification_data);
				
				$this->response_code = 201;
				$this->response['status'] = TRUE;
				$this->response["msg"] = "Successfull registration.";
				$this->response['status_code'] = "NEW_USER";
				$this->response['first_login'] = true;
				$this->response['data'] = $this->transformer->transform($new_user, new UserTransformer,'item');
				DB::commit();

			}else{
				$this->response_code = 507;
				$this->response['msg'] = "Error communicating database.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
			}

			callback:
			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

				default :
				$this->response['msg'] = "Invalid return data format.";
				$this->response['status_code'] = "INVALID_FORMAT";
				$this->response['status'] = FALSE;
				$this->response_code = 406;
			}

		}catch(Exception $e){
			DB::rollback();
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	/**
	 * Registration via facebook
	 * @format return data format
	 * @return Response
	 */
	public function fb_login($format = "", FbInfoRequest $request){
		try{
			$user = User::where('fb_id',$request->get('fb_id'))->whereIn('type',['user'])->first();
			if($user){
				$user->last_login = Carbon::now();
				$user->save();

				$device = UserDevice::where('user_id',$user->id)->where('device_id',Input::get('device_id'))->first();

				if(Input::has('device_id')){
					if(!$device){

						UserDevice::where('device_id',$request->get('device_id'))
									->where('user_id','<>',$user->id)
									->update(['is_login' => "0"]);

						$device = new UserDevice;
						$device->user_id = $user->id;
						$device->reg_id = Input::get('device_reg_id');
						$device->device_id = Input::get('device_id');
						$device->device_name = Input::get('device_name');
						$device->is_login = 1;
						$device->save();

					}else{
						$device->reg_id = Input::get('device_reg_id');
						$device->is_login = 1;
						$device->save();

						UserDevice::where('device_id',$request->get('device_id'))
									->where('user_id','<>',$user->id)
									->update(['is_login' => "0"]);
					}
				}

				$this->response['msg'] = "Successfuly Login";
				$this->response['status_code'] = "AUTH_TRUE";
				$this->response['status'] = TRUE;
				$this->response_code = 200;
				$this->response['data'] = $this->transformer->transform($user, new UserTransformer,'item');

			} else {
				$this->response_code = 422;
				$this->response['status_code'] = "INVALID_FB_ID";
				$this->response['msg'] = "Unknown Facebook Reference ID";

				if($request->get('fb_id')){

					DB::beginTransaction();
					$new_user = new User;
					$new_user->fill($request->all());
					$new_user->type=Str::lower($request->get('user_type'));

					$new_user->type = 'user';
					$new_user->username = $this->_generate_username($request->get('fname'));

					$new_user->fb_id = $request->get('fb_id');
					$new_user->email = $request->get('fb_id')."@facebook.com";
					$new_user->password = bcrypt(uniqid());

					// if($request->hasFile('file')) $new_user->fill(ImageUploader::upload($request, "uploads/users", "file"));

					if($new_user->save()){
						$new_user->last_login = Carbon::now();
						$new_user->save();

						if($request->has('device_id')){
							UserDevice::where('device_id',$request->get('device_id'))
										->where('user_id','<>',$new_user->id)
										->update(['is_login' => "0"]);
							$device = new UserDevice;
							$device->user_id = $new_user->id;
							$device->reg_id = $request->get('device_reg_id');
							$device->device_id = $request->get('device_id');
							$device->device_name = $request->get('device_name');
							$device->is_login = 1;
							$device->save();
						}

						$this->response_code = 201;
						$this->response['status'] = TRUE;
						$this->response['status_code'] = "NEW_USER";
						$this->response['msg'] = "New User";
						$this->response['data'] = $this->transformer->transform($new_user, new UserTransformer,'item');
						DB::commit();
					}else{
						$this->response_code = 507;
						$this->response['status'] = FALSE;
						$this->response['status_code'] = "DB_ERROR";
						$this->response['msg'] = "Unable to store information due to server error. Please try again.";
						DB::rollback();
					}
				}
			}
			
			callback:
			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

				default :
				$this->response['msg'] = "Invalid return data format.";
				$this->response['status_code'] = "INVALID_FORMAT";
				$this->response['status'] = FALSE;
				$this->response_code = 406;
			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['status'] = FALSE;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function remove_email($format = ""){
		try {
			$email = Input::get('email');
			User::where('email',$email)->update(['email' => uniqid() . Helper::date_format(Carbon::now(), "YmdHis") . "@yahoo.com"]);
			$this->response['msg'] = "Email removed Successfully.";
			$this->response['status_code'] = "OVERRIDE_SUCCESS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

				default :
				$this->response['msg'] = "Invalid return data format.";
				$this->response['status_code'] = "INVALID_FORMAT";
				$this->response['status'] = FALSE;
				$this->response_code = 406;
				return response()->json($this->response,$this->response_code);
			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function forgot_password($format = "json", ForgotPasswordRequest $request){
		try{
			$email = Input::get('email');
			$check_email = User::whereRaw("LOWER(email) = '{$email}'")->first();

			$this->response['msg'] = "Your email doesn't belong to any account.";
			$this->response['status_code'] = "INVALID_EMAIL";
			$this->response['status'] = FALSE;
			$this->response_code = 404;

			if($check_email){

				$notification_data = new EmailForgotPassword(['user_id' => $check_email->id]);
				Event::fire('forgot-password', $notification_data);

				$this->response['msg'] = "The validation token for resetting your password has been sent to your email.";
				$this->response['status_code'] = "EMAIL_SENT";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}

			callback:
			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['status'] = FALSE;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function resend_activation($format = "json", ForgotPasswordRequest $request){
		try{
			$email = Input::get('email');
			$check_email = User::whereRaw("LOWER(email) = '{$email}'")->first();

			$this->response['msg'] = "Your email doesn't belong to any account.";
			$this->response['status_code'] = "INVALID_EMAIL";
			$this->response['status'] = FALSE;
			$this->response_code = 404;

			if($check_email){

				$check_email->activation_code = $check_email->activation_code ? : Str::random(40);
				$check_email->save();
				
				$notification_data = new EmailActivationCode(['user_id' => $check_email->id]);
				Event::fire('activation-code', $notification_data);

				$this->response['msg'] = "The activation link has been sent to your email.";
				$this->response['status_code'] = "EMAIL_SENT";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}

			callback:
			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['status'] = FALSE;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function reset_password($format = "json", ResetPasswordRequest $request){
		try{
			$email = $request->get('email');
			$validation_token = $request->get('validation_token');

			$check_email = User::whereRaw("LOWER(email) = '{$email}'")->where('validation_token',$validation_token)->first();

			$this->response['msg'] = "Your token has already expired.";
			$this->response['status_code'] = "TOKEN_EXPIRED";
			$this->response['status'] = FALSE;
			$this->response_code = 409;

			if($check_email){
				$check_email->password = bcrypt($request->get('password'));
				$check_email->validation_token = "";

				$this->response['status_code'] = "DB_ERROR";
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status'] = FALSE;
				$this->response_code = 507;

				if($check_email->save()){
					$this->response['msg'] = "Your new password has been saved successfully.";
					$this->response['status_code'] = "RESET_SUCCESS";
					$this->response['status'] = TRUE;
					$this->response_code = 201;
				}
			}else{
				$check_email = User::whereRaw("LOWER(email) = '{$email}'")->first();

				if(!$check_email){
					$this->response['msg'] = "Unable to proceed process. Email address and token doesn't match.";
					$this->response['status_code'] = "TOKEN_EXPIRED";
					$this->response['status'] = FALSE;
					$this->response_code = 409;
				}

			}

			callback:
			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
					
					
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['status'] = FALSE;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}




}