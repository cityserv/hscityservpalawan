<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Api\Controller;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Queue;
use App\Laravel\Models\QueueForDisplay;
use App\Laravel\Models\User;
use App\Laravel\Models\UserLog;

use App\Laravel\Models\CitizenRequest;
use App\Laravel\Models\CRModule;
use App\Laravel\Models\CRCategory;
use App\Laravel\Models\CRSubCategory;
use App\Laravel\Models\CRProcess;
use App\Laravel\Models\CRTracker;
use App\Laravel\Models\CRTrackerHeader;
use App\Laravel\Models\UserTracker;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Api\QueueRequest;

/**
*
* Additional classes needed by this controller
*/
use Event;
use App\Laravel\Events\LogCitizenRequest;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\QueueTransformer;
use App\Laravel\Transformers\QueueForDisplayTransformer;
use App\Laravel\Transformers\MasterTransformer;

use Input, Str, Helper, Carbon, DB,stdClass;


class QueueController extends Controller{

	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);

		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
	}

	public function index($format = "json") {
		try{

			QueueForDisplay::where('status',"pending")->update(['status' => "completed"]);

			$for_display = Queue::whereIn('status',["for_display","completed","on_going"])
								->whereDate('created_at', "=", Carbon::today()->toDateString())
								->orderBy('teller_id',"DESC")
								->orderBy('sequence_no',"DESC")
								->get();

			$priority_lane = $for_display->where('service_type', "priority_lane")->first();
			$health_service = $for_display->where('service_type', "health_service")->first();
			$scholarship_program = $for_display->where('service_type', "scholarship_program")->first();
			$social_service = $for_display->where('service_type', "social_service")->first();
			$legal_service = $for_display->where('service_type', "legal_service")->first();
			$administrative = $for_display->where('service_type', "administrative")->first();
			$response = [
				'priority_lane' => $priority_lane ? $priority_lane->queue_no : NULL,
				'health_service' => $health_service ? $health_service->queue_no : NULL,
				'scholarship_program' => $scholarship_program ? $scholarship_program->queue_no : NULL,
				'social_service' => $social_service ? $social_service->queue_no : NULL,
				'legal_service' => $legal_service ? $legal_service->queue_no : NULL,
				'administrative' => $administrative ? $administrative->queue_no : NULL,
			];

			$this->response['data'] = $this->transformer->transform($response, new MasterTransformer,'item');
			$this->response['msg'] = "Current Queues.";
			$this->response['status_code'] = "CURRENT_QUEUE_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function for_display($format = "json") {
		try {

			$for_display = QueueForDisplay::where('status',"pending")->get();

			$this->response['data'] = $this->transformer->transform($for_display, new QueueForDisplayTransformer, 'collection');
			$this->response['msg'] = "For Display Queues.";
			$this->response['status_code'] = "FOR_DISPLAY_QUEUE_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function store($format = "json", QueueRequest $request) {
		try{
			$new_queue = new Queue;
			$new_queue->fill($request->all());
			$new_queue->sequence_no = Helper::next_sequence($request->get('service_type'));
			$new_queue->queue_no = Helper::create_queue_no($request->get('service_type'), $new_queue->sequence_no, $request->get('source',"webapp"));
			$new_queue->is_priority = $request->get('is_priority',"no");
			$new_queue->status = "pending";
			$new_queue->source = $request->get('source',"webapp");

			if($new_queue->save()) {
				$this->response['data'] = $this->transformer->transform($new_queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A new queue has been created.";
				$this->response['status_code'] = "QUEUE_CREATED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function flash($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			if(in_array($queue->status, ["on_going","completed","cancelled"]) AND $queue->teller_id) {
				$this->response['msg'] = "Unable to flash, this record is already processed.";
				$this->response['status_code'] = "ALREADY_PROCESSED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			$queue->status = "for_display";
			$queue->teller_id = $teller_id;

			if($queue->save()) {
				$for_display = new QueueForDisplay;
				$for_display->fill(['queue_id' => $queue->id, 'queue_no' => $queue->queue_no, 'service_type' => $queue->service_type, 'status' => "pending", 'is_priority' => $queue->is_priority]);
				$for_display->save();

				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been flashed.";
				$this->response['status_code'] = "QUEUE_FLASHED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function flash_for_display($format = "json") {
		try {

			$for_display_id = Input::get('for_display_id', 0);
			$for_display = QueueForDisplay::find($for_display_id);

			if($for_display->status != "pending") {
				$this->response['msg'] = "Unable to flash, this record is already processed.";
				$this->response['status_code'] = "ALREADY_PROCESSED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			$for_display->status = "completed";

			if($for_display->save()) {

				$queue = Queue::find($for_display->queue_id);
				$for_display->delete();
				
				if($queue->source == "mobile"){
					$split =explode("-", $queue->queue_no);

					$first_letter = substr($split[0], 0,1);
					$second_letter = substr($split[0], 1,1);

					$q_no = "{$first_letter} {$second_letter}-{$split[1]}";

				}else{
					$q_no = $queue->queue_no;
				}

				if ($for_display->type == "serve") {
					if($queue->is_priority == "yes"){

						if($queue){
							$queue_no = "No. {$queue->queue_no}";
							$msg = "Please proceed to <strong>Priority Lane</strong>";
							$txt_speech = "Number {$q_no}, Please proceed to Priority Lane";
						}

					}else{
						$queue_no = " No. " . $queue->queue_no;
						$msg = $queue ?  "Please proceed to <strong>" . Str::title(str_replace("_", " ", $queue->service_type)) . "</strong>" : ("Now serving client " . $queue->queue_no);

						$txt_speech = $queue ? " Number " . $q_no.", Please proceed to ".(Str::title(str_replace("_", " ", $queue->service_type)) ) . "</strong>" : ("Now serving client " . $q_no);
					}
				} else {
					if($queue->is_priority == "yes"){

						if($queue){
							$queue_no = "No. {$queue->queue_no}";
							$msg = "Please Proceed to <strong>Priority Lane</strong>";
							$txt_speech = "Number {$q_no}, Please proceed to Priority Lane ";
						}

					}else{

						$queue_no = "No. " . $queue->queue_no;
						$msg = $queue ? "Please proceed to <strong>". Str::title(str_replace("_", " ", $queue->service_type)) ."</strong>" : ("Now serving client " . $queue->queue_no);
						$txt_speech = $queue ? " Number " . $q_no.", Please proceed to ".(Str::title(str_replace("_", " ", $queue->service_type)) ) : ("Now serving client " . $q_no);
					}
				}
				

				$this->response['queue_no'] = $queue_no;
				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] =  $msg ;
				$this->response['txt_speech'] = $txt_speech;
				$this->response['status_code'] = "QUEUE_FOR_DISPLAY_FLASHED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function start($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			if(in_array($queue->status, ["on_going","completed","cancelled"]) AND $queue->teller_id) {
				$this->response['msg'] = "Unable to start, this record is already processed.";
				$this->response['status_code'] = "ALREADY_PROCESSED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			$queue->teller_id = $teller_id;
			$queue->status = "on_going";

			if($queue->save()) {

				$for_display = new QueueForDisplay;
				$for_display->fill(['queue_id' => $queue->id, 'queue_no' => $queue->queue_no, 'service_type' => $queue->service_type, 'status' => "pending", 'is_priority' => $queue->is_priority, 'type' => "serve"]);
				$for_display->save();

				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been started.";
				$this->response['status_code'] = "QUEUE_STARTED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function stop($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			if($queue->status != "on_going") {
				$this->response['msg'] = "Unable to stop, queue has not started yet.";
				$this->response['status_code'] = "QUEUE_NOT_STARTED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			if($queue->teller_id != $teller_id) {
				$this->response['msg'] = "Teller is unauthorized to stop this queue.";
				$this->response['status_code'] = "UNAUTHORIZED_ACCESS";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;	
			}

			$queue->teller_id = $teller_id;
			$queue->status = "completed";

			if($queue->save()) {
				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been completed.";
				$this->response['status_code'] = "QUEUE_COMPLETED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function cancel($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			if(in_array($queue->status, ["on_going","completed","cancelled"]) AND $queue->teller_id) {
				$this->response['msg'] = "Unable to cancel, this record is already processed.";
				$this->response['status_code'] = "ALREADY_PROCESSED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			$queue->teller_id = $teller_id;
			$queue->status = "cancelled";

			if($queue->save()) {
				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been cancelled.";
				$this->response['status_code'] = "QUEUE_CANCELLED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function prioritize($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			$queue->is_priority = "yes";
			$queue->teller_id = $teller_id;

			if($queue->save()) {
				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been prioritized.";
				$this->response['status_code'] = "QUEUE_PRIORITIZED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function transfer($format = "json") {
		try {

			$queue_id = Input::get("queue_id", 0);
			$teller_id = Input::get("teller_id", 0);

			$queue = Queue::find($queue_id);

			if($queue->status != "on_going" AND in_array($queue->service_type, ['social_service','scholarship'])) {
				$this->response['msg'] = "Unable to transfer, queue has not started yet.";
				$this->response['status_code'] = "QUEUE_NOT_STARTED";
				$this->response['status'] = FALSE;
				$this->response_code = 409;
				goto callback;
			}

			$queue->teller_id = $teller_id;
			$queue->is_transfer = "yes";

			if($queue->save()) {

				// Create citizen request id
				$subcategory = Input::get('subcategory');
				$name = $queue->name;

				$cr_subcategory = CRSubcategory::where('code', $subcategory)->first();
				if ($cr_subcategory) {
					$cr_processes = CRProcess::where('cr_subcategory_id', $cr_subcategory->id)->orderBy('sequence',"ASC")->get();
					$cr_category = CRCategory::where('id', $cr_subcategory->cr_category_id)->first();
					$cr_module = CRModule::where('id', $cr_category->cr_module_id)->first();

					// Create new citizen request
					$new_request = new CitizenRequest;
					$new_request->name = $name;
					$new_request->beneficiary_name = $name;
					$new_request->module = $cr_module->code;
					$new_request->category = $cr_category->code;
					$new_request->subcategory = $cr_subcategory->code;
					$new_request->user_id = 0;
					$new_request->status = "pending";
					$new_request->remarks = "Request created.";
					$new_request->title = $cr_subcategory->title;
					$new_request->code = Helper::create_request_code($cr_category->id, Carbon::now());

					$suffix = $cr_subcategory->id; //For now, use the primary key of the subcategory to determine the suffix
					$new_request->year = Carbon::now()->format('y');
					$new_request->sequence_no = Helper::next_tracking_sequence($new_request->subcategory, $new_request->year);
					$new_request->tracking_number = Helper::create_tracking_number(['year' => $new_request->year, 'sequence_no' => $new_request->sequence_no, 'suffix' => $suffix]);

					$new_request->save();

					// Create tracker header
					$header = new CRTrackerHeader;
					$header->citizen_request_id = $new_request->id;
					$header->cr_category_id = $cr_category->id;
					$header->cr_subcategory_id = $cr_subcategory->id;
					$header->category_title = $cr_category->title;
					$header->category_code = $cr_category->code;
					$header->subcategory_title = $cr_subcategory->title;
					$header->subcategory_code = $cr_subcategory->code;
					$header->appointment_schedule = Carbon::now(); 
					$header->save();

					// Create copy of processes to tracker table
					foreach ($cr_processes as $index => $process) {
						$tracker = new CRTracker;
						$tracker->cr_tracker_header_id = $header->id;
						$tracker->cr_process_id = $process->id;
						$tracker->process_title = $process->title;
						$tracker->process_code = $process->code;
						if($index == 0) $tracker->from = Carbon::now();
						$tracker->save();
					}

					$data = new LogCitizenRequest(['citizen_request_id' => $new_request->id, 'data' => ['user_id' => $this->user_id, 'remarks' => $new_request->remarks]]);
					Event::fire('log-citizen-request', $data);
				}


				$this->response['data'] = $this->transformer->transform($queue, new QueueTransformer, 'item');
				$this->response['msg'] = "A queue has been transferred.";
				$this->response['status_code'] = "QUEUE_TRANSFERRED";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function get_service_data($format = "json"){
		try{
			$service_type = Input::get('_type');
			$excluded_id = explode(",",Input::get('_ids',"0"));
			$current_date = Helper::date_db(Carbon::now());

			if($service_type == "priority_lane") {
				$list = Queue::whereNotIn("id",$excluded_id)->whereRaw("DATE(created_at) = '{$current_date}'")->where('is_priority',"yes")->where(function($query){
					return $query->where('status','pending')
								 ->orWhere('status','for_display')
								 ->orWhere('status','on_going');

					})->orderBy('created_at',"ASC")->get();
			} else {
				$list = Queue::whereNotIn("id",$excluded_id)->whereRaw("DATE(created_at) = '{$current_date}'")->where('service_type',$service_type)->where(function($query){
					return $query->where('status','pending')
								 ->orWhere('status','for_display')
								 ->orWhere('status','on_going');

					})->orderBy('created_at',"ASC")->get();	
			}
			

			$this->response['data'] = $this->transformer->transform($list, new QueueTransformer, 'collection');
			$this->response['msg'] = "Incoming queue.";
			$this->response['status_code'] = "INCOMING_QUEUE";
			$this->response['status'] = TRUE;
			$this->response_code = 200;
			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function get_last_data($format = "json"){
		try{
			$current_date = Helper::date_db(Carbon::now());
			$this->response['total_queue'] = Queue::whereRaw("DATE(created_at) = '{$current_date}'")->count();
			$this->response['most_queue'] = Queue::select(DB::raw("COUNT(*) as sum_queue"),"service_type")->whereRaw("DATE(created_at) = '{$current_date}'")->groupBy("service_type")->orderByRaw("sum_queue DESC")->first();

			if(!$this->response['most_queue']){
				$this->response['most_queue'] = new stdClass;
				$this->response['most_queue']->service_type = "social_service";
				$this->response['most_queue']->sum_queue = 0;
			}

			$recent_queue = Queue::whereRaw("DATE(created_at) = '{$current_date}'")->orderBy('created_at',"DESC")->take(5)->get();
			$this->response['recent_queue'] = $this->transformer->transform($recent_queue, new QueueTransformer, 'collection');
			$this->response['msg'] = "Updated data list.";
			$this->response['status_code'] = "UPDATED_DATA";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function on_queue($format = "json"){
		try{
			$service_type = Input::get('_type');
			$excluded_id = explode(",",Input::get('_ids',"0"));
			$current_date = Helper::date_db(Carbon::now());

			if($service_type == "priority_lane") {
				$list = Queue::whereIn("id",$excluded_id)->whereRaw("DATE(created_at) = '{$current_date}'")->where('is_priority',"yes")->orderBy('created_at',"ASC")->get();
			} else {
				$list = Queue::whereIn("id",$excluded_id)->whereRaw("DATE(created_at) = '{$current_date}'")->where('service_type',$service_type)->orderBy('created_at',"ASC")->get();	
			}

			$this->response['data'] = $this->transformer->transform($list, new QueueTransformer, 'collection');
			$this->response['msg'] = "Ongoing queue.";
			$this->response['status_code'] = "ONGOING_QUEUE";
			$this->response['status'] = TRUE;
			$this->response_code = 200;
			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;

			}

		} catch (Exception $e) {
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}
