<?php

namespace App\Laravel\Controllers\Api;

use App\Laravel\Controllers\Api\Controller;
use App\Laravel\Models\User;
use App\Laravel\Models\UserDevice;

use App\Laravel\Transformers\TransformerManager;
use App\Laravel\Transformers\UserTransformer;

use App\Laravel\Requests\Api\AvatarRequest;
use App\Laravel\Requests\Api\PasswordRequest;
use App\Laravel\Requests\Api\ProfileRequest;
use App\Laravel\Requests\Api\UserFieldRequest;

use Helper,ImageUploader,Carbon,Session,Input,Str,Auth,AzureStorage,DB;
// use Agent,GeoIP,Request;

// use App\Knowheretogo\Events\PushNotification;
// use Event;

use Event,Request,GeoIP;
// use App\Kcrops\Events\LogLastActivity;



class UserController extends Controller{
	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id');
		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	public function profile($format = "json"){
		try{
			$user_id = Input::get('user_id',0);
			$user = User::find($user_id);
			$this->response['data'] = $this->transformer->transform($user,new UserTransformer,'item');
			$this->response['msg'] = "User Profile.";
			$this->response['status_code'] = "USER_PROFILE";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}	
	}

	public function update_profile($format = "json", ProfileRequest $request){
		try{
			// $user_id = Input::get('user_id',0);
			$user = User::find($this->user_id);
			$user->fill($request->only(['fname','lname','email','contact_number','age','gender']));

			if($user->save()){
				$this->response['data'] = $this->transformer->transform($user,new UserTransformer,'item');
				$this->response['msg'] = "Profile updated successfully.";
				$this->response['status_code'] = "PROFILE_UPDATE";
				$this->response['status'] = TRUE;
				$this->response_code = 200;
			}else{
				$this->response['status'] = FALSE;
				$this->response['msg'] = "Database server communication error.";
				$this->response['status_code']	= "DB_ERROR";
				$this->response_code = 504;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}	
	}

	public function update_field(UserFieldRequest $request,$format = "json"){
		try{
			// $user_id = Input::get('user_id',0);
			$user_id = Input::get("user_id");
			$field = Input::get('field');
			$value = Input::get('value');
			$title = Input::get('nice_field');

			// $user = User::find($user_id);
			
			
			$user = User::find($this->user_id);
			$user->{$field} = $value;
			if($user->save()){
				$this->response['data'] = $this->transformer->transform($user,new UserTransformer,'item');
				$this->response['msg'] = "Profile updated successfully.";
				$this->response['status_code'] = "PROFILE_UPDATE";
				$this->response['status'] = TRUE;
				$this->response_code = 200;
			}else{
				$this->response['status'] = FALSE;
				$this->response['msg'] = "Database server communication error.";
				$this->response['status_code']	= "DB_ERROR";
				$this->response_code = 504;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}	

	}

	public function update_password($format = "json", PasswordRequest $request){
		try{
			// $user_id = Input::get('user_id',0);
			$user = User::find($this->user_id);
			$user->password = bcrypt($request->get('password'));

			if($user->save()){
				$this->response['data'] = $this->transformer->transform($user,new UserTransformer,'item');
				$this->response['msg'] = "Profile updated successfully.";
				$this->response['status_code'] = "PROFILE_UPDATE";
				$this->response['status'] = TRUE;
				$this->response_code = 200;
			}else{
				$this->response['status'] = FALSE;
				$this->response['msg'] = "Database server communication error.";
				$this->response['status_code']	= "DB_ERROR";
				$this->response_code = 504;
			}

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}	
	}

	public function update_avatar($format = "json", AvatarRequest $request){
		try{
			// $user_id = Input::get('user_id',0);
			$user = User::find($this->user_id);
			// $file = $request->file('file');
			// $ext = $file->getClientOriginalExtension();
			$orig_image_path = $user->path;
			$orig_filename = $user->filename;

			if($request->hasFile('file')) $user->fill(ImageUploader::upload($request, "uploads/users", "file"));

			// $path_directory = 'upload/user/'.Helper::date_format(Carbon::now(),"Ymd");
			// $resized_directory = 'upload/user/'.Helper::date_format(Carbon::now(),"Ymd")."/resized";
			// $thumb_directory = 'upload/user/'.Helper::date_format(Carbon::now(),"Ymd")."/thumbnails";

			// if (!File::exists($path_directory)){
			// 	File::makeDirectory($path_directory, $mode = 0777, true, true);
			// }

			// if (!File::exists($resized_directory)){
			// 	File::makeDirectory($resized_directory, $mode = 0777, true, true);
			// }

			// if (!File::exists($thumb_directory)){
			// 	File::makeDirectory($thumb_directory, $mode = 0777, true, true);
			// }

			// $filename = Helper::create_filename($ext);
			// $user->filename = $filename;
			// $file->move($path_directory, $filename); 

			// if(Image::make("{$path_directory}/{$filename}")->width() > Image::make("{$path_directory}/{$filename}")->height()){
			// 	Image::make("{$path_directory}/{$filename}")->resize(null, 512, function ($constraint) {
			// 	    $constraint->aspectRatio();
			// 	})->orientate()->save("{$resized_directory}/{$filename}",95);
			// 	Image::make("{$path_directory}/{$filename}")->resize(null, 256, function ($constraint) {
			// 	    $constraint->aspectRatio();
			// 	})->orientate()->save("{$thumb_directory}/{$filename}",90);

			// }else{
			// 	Image::make("{$path_directory}/{$filename}")->resize(512, null, function ($constraint) {
			// 	    $constraint->aspectRatio();
			// 	})->orientate()->save("{$resized_directory}/{$filename}",95);
			// 	Image::make("{$path_directory}/{$filename}")->resize(256, null, function ($constraint) {
			// 	    $constraint->aspectRatio();
			// 	})->orientate()->save("{$thumb_directory}/{$filename}",90);
			// }
			
			// $client = new AzureStorage(env('BLOB_STORAGE_URL'),env('BLOB_ACCOUNT_NAME'),env('BLOB_ACCESS_KEY'));
			
			// $container= env('BLOB_CONTAINER');
			// $orig_container = env('BLOB_ORIG_CONTAINER');
			
			// $directory = env('BLOB_STORAGE_URL')."/".env('BLOB_CONTAINER');
			
			// $user->directory = "{$directory}/".str_replace("upload/", "", $path_directory); 
			// $user->path = str_replace("upload/", "", $path_directory);

			// $client->putBlob($orig_container, "{$user->path}/{$filename}", "{$path_directory}/{$filename}");
			// $client->putBlob($container, "{$user->path}/thumbnails/{$filename}", "{$path_directory}/thumbnails/{$filename}");
			// $client->putBlob($container, "{$user->path}/resized/{$filename}", "{$path_directory}/resized/{$filename}");
			
			// if (File::exists("{$path_directory}/{$filename}")){
			// 	File::delete("{$path_directory}/{$filename}");
			// }
			// if (File::exists("{$path_directory}/thumbnails/{$filename}")){
			// 	File::delete("{$path_directory}/thumbnails/{$filename}");
			// }
			// if (File::exists("{$path_directory}/resized/{$filename}")){
			// 	File::delete("{$path_directory}/resized/{$filename}");
			// }


			if($user->save()){

				// $client = new AzureStorage(env('BLOB_STORAGE_URL'),env('BLOB_ACCOUNT_NAME'),env('BLOB_ACCESS_KEY'));
				// $container= env('BLOB_CONTAINER');
				// $orig_container = env('BLOB_ORIG_CONTAINER');
				// $directory = env('BLOB_STORAGE_URL')."/".env('BLOB_CONTAINER');
				// $client->deleteBlob($orig_container, "{$orig_image_path}/{$orig_filename}");
				// $client->deleteBlob($container, "{$orig_image_path}/resized/{$orig_filename}");
				// $client->deleteBlob($container, "{$orig_image_path}/thumbnails/{$orig_filename}");

				$this->response['data'] = $this->transformer->transform($user,new UserTransformer,'item');
				$this->response['msg'] = "Profile updated successfully.";
				$this->response['status_code'] = "AVATAR_UPDATE";
				$this->response['status'] = TRUE;
				$this->response_code = 200;
			}else{
				$this->response['status'] = FALSE;
				$this->response['msg'] = "Database server communication error.";
				$this->response['status_code']	= "DB_ERROR";
				$this->response_code = 504;

			}

			callback:
			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function update_reg_id($format = "json"){
		try{
			$user_id = Input::get('user_id',0);
			$device_id = Input::get('device_id',0);
			$device_reg_id = Input::get('device_reg_id',"");

			UserDevice::where('user_id',$user_id)->where('device_id',$device_id)->update(['reg_id' => $device_reg_id]);

			$this->response['msg'] = "Registration token updated successfully.";
			$this->response['status_code'] = "REG_ID_UPDATE";
			$this->response['status'] = TRUE;
			$this->response_code = 200;
			

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}	
	}


}