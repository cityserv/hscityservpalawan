<?php 

namespace App\Laravel\Controllers\Api;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\CitizenReport;
use App\Laravel\Models\Emergency;

/**
*
* Requests used for this controller
*/
use App\Laravel\Requests\Api\CitizenReportRequest;

/**
*
* Transformers used for this controller
*/
use App\Laravel\Transformers\CitizenReportTransformer;

/**
*
* Classes used for this controller
*/
use App\Laravel\Transformers\MasterTransformer;
use App\Laravel\Transformers\TransformerManager;
use Helper, Carbon, Input, Str, ImageUploader;
use Request, GeoIp;

class CitizenReportController extends Controller{

	protected $response;

	public function __construct(){
		$this->user_id = Input::get('auth_id',0);
		$this->response = array(
				"msg" => "Bad Request.",
				"status" => FALSE,
				'status_code' => "UNAUTHORIZED"
			);
		$this->response_code = 401;
		$this->transformer = new TransformerManager;
	}

	public function types($format = "json") {
		$types = Emergency::orderBy('title', "ASC")->pluck("title")->toArray();
		$this->response['data'] = $this->transformer->transform($types, new MasterTransformer, 'item');
		$this->response['msg'] = "Emergency Types List.";
		$this->response['status_code'] = "EMERGENCY_TYPES_LIST";
		$this->response['status'] = TRUE;
		$this->response_code = 200;

		switch(Str::lower($format)){
			case 'json' :
				return response()->json($this->response,$this->response_code);
			break;
				
			default :
				$this->response['msg'] = "Invalid return data format.";
				$this->response['status_code'] = "INVALID_FORMAT";
				$this->response['status'] = FALSE;
				$this->response_code = 406;
				return response()->json($this->response,$this->response_code);
		}
	}

	public function index($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$reports = CitizenReport::where('status','<>',"pending")
					->with('author')->orderBy('created_at',"DESC")
					->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($reports, new CitizenReportTransformer, 'collection');
			$this->response['has_morepage'] = $reports->hasMorePages();
			$this->response['msg'] = "Citizen Report List.";
			$this->response['status_code'] = "CITIZEN_REPORT_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function my_reports($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$reports = CitizenReport::with('author')
						->where('user_id',$this->user_id)
						->orderBy('created_at',"DESC")->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($reports, new CitizenReportTransformer, 'collection');
			$this->response['has_morepage'] = $reports->hasMorePages();
			$this->response['msg'] = "My Report List.";
			$this->response['status_code'] = "MY_REPORT_LIST";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function show($format = "json"){
		try{
			$report = CitizenReport::with('author')
						->where('id',Input::get('report_id',0))
						->first();

			$this->response['data'] = $this->transformer->transform($report, new CitizenReportTransformer, 'item');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function pending($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$report = CitizenReport::where('user_id',$this->user_id)
						->where('status',"pending")
						->orderBy('created_at',"DESC")
						->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($report, new CitizenReportTransformer, 'collection');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function on_going($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$report = CitizenReport::where('user_id',$this->user_id)
						->where('status',"on_going")
						->orderBy('created_at',"DESC")
						->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($report, new CitizenReportTransformer, 'collection');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function resolved($format = "json"){
		try{
			$per_page = Input::get('per_page',10);
			$report = CitizenReport::where('user_id',$this->user_id)
						->where('status',"resolved")
						->orderBy('created_at',"DESC")
						->paginate($per_page);

			$this->response['data'] = $this->transformer->transform($report, new CitizenReportTransformer, 'collection');
			$this->response['msg'] = "Report  Details.";
			$this->response['status_code'] = "MY_REPORT_DETAILS";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function store(CitizenReportRequest $request, $format = "json"){
		try{	
			$new_report = new CitizenReport;
			$new_report->fill($request->all());

			$new_report->user_id = $this->user_id;

			$user_lat = round(Input::get('user_lat'),6);
			$user_long = round(Input::get('user_long'),6);

			if($user_lat AND $user_long){

			}else{
				$ip = Request::header('X-Forwarded-For');
				if(!$ip){
					$ip = Request::getClientIp();
				}
				
				$location = GeoIP::getLocation($ip);
				$user_lat = $location['lat'];
				$user_long = $location['lon'];
			}

			$new_report->geo_lat = $user_lat;
			$new_report->geo_long = $user_long;

			if($request->hasFile('file')) $new_report->fill(ImageUploader::upload($request, "uploads/reports", "file"));

			if($new_report->save()) {
				$new_report->code = "RPT" . str_pad($new_report->id, 8, 0, STR_PAD_LEFT);
				$new_report->save();
				$this->response['msg'] = "Your report has been sent";
				$this->response['status_code'] = "REPORT_SENT";
				$this->response['status'] = TRUE;
				$this->response_code = 201;
			}else{
				$this->response['msg'] = "Unable to store information due to server error. Please try again.";
				$this->response['status_code'] = "DB_ERROR";
				$this->response['status'] = FALSE;
				$this->response_code = 507;
			}

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
			}
		}
		catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

	public function destroy($format = "json"){
		try{
			$report = CitizenReport::with('author')
						->where('id',Input::get('report_id',0))
						->first();

			$report->delete();

			$this->response['data'] = $this->transformer->transform($report, new CitizenReportTransformer, 'item');
			$this->response['msg'] = "Report  Deleted.";
			$this->response['status_code'] = "REPORT_DELETED";
			$this->response['status'] = TRUE;
			$this->response_code = 200;

			callback:

			switch(Str::lower($format)){
				case 'json' :
					return response()->json($this->response,$this->response_code);
				break;
					
				default :
					$this->response['msg'] = "Invalid return data format.";
					$this->response['status_code'] = "INVALID_FORMAT";
					$this->response['status'] = FALSE;
					$this->response_code = 406;
					return response()->json($this->response,$this->response_code);
			}
		}catch(Exception $e){
			$this->response_code = 500;
			$this->response['msg']	= $e->getMessage();
			$this->response['status_code'] = "ERROR_EXCEPTION";
			return response()->json($this->response,$this->response_code);
		}
	}

}