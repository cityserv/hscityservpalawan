<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\User;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\AdminRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class AdminController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['types'] = [ '' => "Choose type",
			//'super_user' => "Super User",
			'admin' => "Administrator",
			'receptionist' => "Receptionist",
			'tv' => "TV",
			'teller' => "Teller" ];

		if($this->data['auth']->type == "admin"){
			$this->data['types'] = [ '' => "Choose type", 'admin' => "Administrator", 'receptionist' => "Receptionist", 'tv' => "TV", 'teller' => "Teller" ];
		}
	}

	private function _generate_username($str){
		$new_username = Helper::str_clean(Str::lower($str));
		$check_username = User::where('username', 'like', "%{$str}%")->withTrashed()->count();
		if($check_username) $new_username = $new_username.($check_username + 1);
		return $new_username;
	}

	public function index () {
		if($this->data['auth']->type == "admin"){
			$this->data['users'] = User::whereIn('type',["employee"])->orderBy('updated_at',"DESC")->get();
		}else{
			$this->data['users'] = User::where('id', "<>", 1)->whereIn('type',["super_user","admin","receptionist","tv","teller"])->orderBy('updated_at',"DESC")->get();
		}
		return view('backoffice.admins.index',$this->data);
	}

	public function create () {
		return view('backoffice.admins.create',$this->data);
	}

	public function store (AdminRequest $request) {
		try {
			$new_user = new User;
			$new_user->fill($request->all());
			$new_user->username = $this->_generate_username($request->get('fname'));
			$new_user->email = Str::lower($request->get('email'));
			$new_user->password = bcrypt($request->get('password'));
			$new_user->type = $request->get('type');

			if($request->hasFile('file')) $new_user->fill(ImageUploader::upload($request, 'uploads/admin',"file"));

			if($new_user->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An admin has been added.");
				return redirect()->route('backoffice.admins.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$admin = User::whereIn('type',["super_user","admin","receptionist","tv","teller"])->find($id);

		if (!$admin) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.admins.index');
		}

		$this->data['user'] = $admin;
		return view('backoffice.admins.edit',$this->data);
	}

	public function update (AdminRequest $request, $id = NULL) {
		try {
			$admin = User::whereIn('type',["super_user","admin","receptionist","tv","teller"])->find($id);

			if (!$admin) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.admins.index');
			}

			$admin->fill($request->all());
			$admin->email = Str::lower($request->get('email'));
			if($request->hasFile('file')) $admin->fill(ImageUploader::upload($request, 'uploads/admin',"file"));

			if($request->get('password', FALSE)) {
				$admin->password = bcrypt($request->get('password'));
			}
			
			if($admin->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An admin has been updated.");
				return redirect()->route('backoffice.admins.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$admin = User::whereIn('type',["super_user","admin","receptionist","tv","teller"])->find($id);

			if (!$admin) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.admins.index');
			}

			if($admin->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An admin has been deleted.");
				return redirect()->route('backoffice.admins.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}