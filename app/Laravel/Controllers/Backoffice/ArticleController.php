<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\Article;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\ArticleRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class ArticleController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['articles'] = Article::orderBy('updated_at',"DESC")->get();
		return view('backoffice.articles.index',$this->data);
	}

	public function create () {
		return view('backoffice.articles.create',$this->data);
	}

	public function store (ArticleRequest $request) {
		try {
			$new_article = new Article;
			$new_article->fill($request->all());
			$new_article->user_id = $this->data["auth"]->id;
			$new_article->posted_at = Helper::datetime_db($request->get('posted_at'));
			$new_article->slug = Helper::get_slug('article','title',$request->get('title'));
			$new_article->excerpt = Helper::get_excerpt($request->get('content'));
			$new_article->featured = ($request->get('featured',0) == 1) ? 1 : 0;
			if($request->hasFile('file')) $new_article->fill(ImageUploader::upload($request, 'uploads/article',"file"));

			if($new_article->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An article has been added.");
				return redirect()->route('backoffice.articles.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$article = Article::find($id);

		if (!$article) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.articles.index');
		}

		$this->data['article'] = $article;
		return view('backoffice.articles.edit',$this->data);
	}

	public function update (ArticleRequest $request, $id = NULL) {
		try {
			$article = Article::find($id);

			if (!$article) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.articles.index');
			}

			$old_title = $article->title;

			$article->fill($request->all());
			$article->posted_at = Helper::datetime_db($request->get('posted_at'));
			$article->excerpt = Helper::get_excerpt($request->get('content'));
			$article->featured = ($request->get('featured',0) == 1) ? 1 : 0;
			
			if($old_title != $request->get('title')){
				$article->slug = Helper::get_slug('article','title',$request->get('title'));
			}
			
			if($request->hasFile('file')) $article->fill(ImageUploader::upload($request, 'uploads/article',"file"));

			if($article->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An article has been updated.");
				return redirect()->route('backoffice.articles.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$article = Article::find($id);

			if (!$article) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.articles.index');
			}

			if($article->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"An article has been deleted.");
				return redirect()->route('backoffice.articles.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}