<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\AppSetting;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\AppSettingRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class AppSettingController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['types'] = [ '' => "Choose type", 'text' => "Text", 'image' => "Image"];
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['settings'] = AppSetting::orderBy('updated_at',"DESC")->get();
		return view('backoffice.app-settings.index',$this->data);
	}

	public function create () {
		return view('backoffice.app-settings.create',$this->data);
	}

	public function store (AppSettingRequest $request) {
		try {
			$new_setting = new AppSetting;
			$new_setting->fill($request->all());
			if($request->hasFile('file')) $new_setting->fill(ImageUploader::upload($request, 'uploads/app-settings',"file"));

			if($new_setting->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A setting has been added.");
				return redirect()->route('backoffice.app_settings.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$app_setting = AppSetting::find($id);

		if (!$app_setting) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.app_settings.index');
		}

		$this->data['setting'] = $app_setting;
		return view('backoffice.app-settings.edit',$this->data);
	}

	public function update (AppSettingRequest $request, $id = NULL) {
		try {
			$app_setting = AppSetting::find($id);

			if (!$app_setting) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.app_settings.index');
			}

			$app_setting->fill($request->all());
			if($request->hasFile('file')) $app_setting->fill(ImageUploader::upload($request, 'uploads/app-settings',"file"));

			if($app_setting->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A setting has been updated.");
				return redirect()->route('backoffice.app_settings.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$app_setting = AppSetting::find($id);

			if (!$app_setting) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.app_settings.index');
			}

			if (in_array($app_setting->code, ['version_name','major_version','minor_version','changelogs'])) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Permission denied. This record cannot be deleted.");
				return redirect()->route('backoffice.app_settings.index');
			}

			if($app_setting->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A setting has been deleted.");
				return redirect()->route('backoffice.app_settings.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}