<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\PriorAdoption;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\PriorAdoptionRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class PriorAdoptionController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = ['pending' => "Pending", 'approved' => "Approved"];
	}

	public function index () {
		$this->data['requests'] = PriorAdoption::orderBy('updated_at',"DESC")->get();
		return view('backoffice.prior-adoption-requests.index',$this->data);
	}

	public function edit ($id = NULL) {
		$request = PriorAdoption::find($id);

		if (!$request) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.prior_adoption_requests.index');
		}

		$this->data['request'] = $request;
		return view('backoffice.prior-adoption-requests.edit',$this->data);
	}

	public function update (PriorAdoptionRequest $request, $id = NULL) {
		try {
			$certificate = PriorAdoption::find($id);

			if (!$request) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.prior_adoption_requests.index');
			}

			$certificate->fill($request->all());
			// $request->posted_at = Helper::datetime_db($request->get('posted_at'));
			
			// if($request->hasFile('file')) $request->fill(ImageUploader::upload($request, 'uploads/requests',"file"));

			if($certificate->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A request has been updated.");
				return redirect()->route('backoffice.prior_adoption_requests.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$request = PriorAdoption::find($id);

			if (!$request) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.prior_adoption_requests.index');
			}

			if($request->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A request has been deleted.");
				return redirect()->route('backoffice.prior_adoption_requests.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}