<?php 

namespace App\Laravel\Controllers\Backoffice;

/**
*
* Models used for this controller
*/
use App\Laravel\Models\School;

/**
*
* Requests used for validating inputs
*/
use App\Laravel\Requests\Backoffice\SchoolRequest;

/**
*
* Additional classes needed by this controller
*/
use Helper, ImageUploader, Carbon, Session, Str, DB;

class SchoolController extends Controller{


	/**
	*
	* @var array $data
	*/
	protected $data;

	public function __construct () {
		$this->data = [];
		parent::__construct();
		array_merge($this->data, parent::get_data());
		$this->data['statuses'] = [ '' => "Choose status", 'draft' => "Draft", 'published' => "Published"];
	}

	public function index () {
		$this->data['schools'] = School::orderBy('updated_at',"DESC")->get();
		return view('backoffice.schools.index',$this->data);
	}

	public function create () {
		return view('backoffice.schools.create',$this->data);
	}

	public function store (SchoolRequest $request) {
		try {
			$new_school = new School;
			$new_school->fill($request->all());

			if($new_school->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A school has been added.");
				return redirect()->route('backoffice.schools.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

			return redirect()->back();
		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function edit ($id = NULL) {
		$school = School::find($id);

		if (!$school) {
			Session::flash('notification-status',"failed");
			Session::flash('notification-msg',"Record not found.");
			return redirect()->route('backoffice.schools.index');
		}

		$this->data['school'] = $school;
		return view('backoffice.schools.edit',$this->data);
	}

	public function update (SchoolRequest $request, $id = NULL) {
		try {
			$school = School::find($id);

			if (!$school) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.schools.index');
			}

			$school->fill($request->all());

			if($school->save()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A school has been updated.");
				return redirect()->route('backoffice.schools.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}

	public function destroy ($id = NULL) {
		try {
			$school = School::find($id);

			if (!$school) {
				Session::flash('notification-status',"failed");
				Session::flash('notification-msg',"Record not found.");
				return redirect()->route('backoffice.schools.index');
			}

			if($school->delete()) {
				Session::flash('notification-status','success');
				Session::flash('notification-msg',"A school has been deleted.");
				return redirect()->route('backoffice.schools.index');
			}

			Session::flash('notification-status','failed');
			Session::flash('notification-msg','Something went wrong.');

		} catch (Exception $e) {
			Session::flash('notification-status','failed');
			Session::flash('notification-msg',$e->getMessage());
			return redirect()->back();
		}
	}
}