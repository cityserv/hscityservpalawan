<?php

namespace App\Laravel\Middleware\Api;

use Closure;
use Illuminate\Contracts\Auth\Guard;

use App\Laravel\Models\Queue;
use Input;

class ValidQueue {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
		// $this->cache_expiration = Helper::get_cache_expiry();
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{

		$queue_id = Input::get('queue_id',0);
		$queue = Queue::select("id")->where('id',$queue_id)->first();

		if(!$queue){
			$response = array(
					"msg" => "Record not found.",
					"status" => FALSE,
					'status_code' => "RECORD_NOT_FOUND"
				);
			$response_code = 404;

			return response($response, $response_code);
		}

		return $next($request);
	}

}
