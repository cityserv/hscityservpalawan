<?php 

namespace App\Laravel\Middleware\Backoffice;

use Closure, Session;
use Illuminate\Contracts\Auth\Guard;

use App\Laravel\Models\CitizenRequest;

class ZoneVerifier {

	/**
	 * The Guard implementation.
	 *
	 * @var Guard
	 */
	protected $auth;

	/**
	 * Create a new filter instance.
	 *
	 * @param  Guard  $auth
	 * @return void
	 */
	public function __construct(Guard $auth)
	{
		$this->auth = $auth;
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		$request_id = isset($request->id) ? $request->id : 0;
		$citizen_request = CitizenRequest::find($request_id);
		
		// if (!in_array($this->auth->user()->type, ['super_user','admin'])) {
		if($this->auth->user()->zone AND $this->auth->user()->zone != "0"){
			if($citizen_request->zone != $this->auth->user()->zone) {
				Session::flash('notification-status','failed');
				Session::flash('notification-msg',"You don't have enough access to view the chosen request.");
				return redirect()->back();
			}
		}

		return $next($request);
	}

}