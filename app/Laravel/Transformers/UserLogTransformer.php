<?php 

namespace App\Laravel\Transformers;

use App\Laravel\Models\UserLog;
use App\Laravel\Models\CitizenRequest;

use League\Fractal\Manager;
use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;

use League\Fractal\TransformerAbstract;
use DB,Helper,Str,Cache,Carbon,DateTime,Input;

class UserLogTransformer extends TransformerAbstract{

	protected $availableIncludes = [
        'date'
    ];

	public function transform(UserLog $log){

        $citizen_request = CitizenRequest::find($log->reference_id);

	 	return [
	 		'user_id' => $log->user_id,
    		'msg' => ($citizen_request ? "Tracking #{$citizen_request->tracking_number}\r\n" : NULL) . $log->msg ,
    		'type' => $log->type,
    		'reference_id' => $log->reference_id,
    		'title' => $log->title,
    		'is_read' => $log->is_read,
            'image' => $log->thumbnail ? : "",
    	];
	}

    public function includeDate(UserLog $log){
        $collection = Collection::make([
                    'created_at'    => [
                        'date_db'       => strtotime($log->created_at) ? $log->date_db($log->created_at) : "",
                        'month_year'    => strtotime($log->created_at) ? $log->month_year($log->created_at) : "",
                        'time_passed'   => strtotime($log->created_at) ? $log->time_passed($log->created_at) : "",
                        'timestamp'     => strtotime($log->created_at) ? $log->created_at : ""
                    ],
            ]);

        return $this->item($collection,New MasterTransformer);
    }

}