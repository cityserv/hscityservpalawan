<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\MarriageCertificate;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class MarriageCertificateTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'info','date','user'
    ];

	public function transform(MarriageCertificate $request){
	     return [
	     	'id' => $request->id,
	     	'code' => $request->code,
	     	'husband_last_name'	=> $request->husband_lname,
	     	'husband_first_name' => $request->husband_fname,
	     	'husband_middle_name' => $request->husband_middle_name,
	     	'wife_last_name'	=> $request->wife_lname,
	     	'wife_first_name' => $request->wife_fname,
	     	'wife_middle_name' => $request->wife_middle_name,
	     	'date_of_marriage' => $request->date_of_marriage,
	     	'place_of_marriage' => $request->place_of_marriage,
	     	'contact_number' => $request->contact,
	     	'purpose' => $request->purpose,
	     	'number_of_copies' => $request->number_of_copies,
	     	
	     	'requested_by' => $request->author ? : new User,
	     ];
	}

	public function includeDate(MarriageCertificate $request){
        $collection = Collection::make([
			'date_db' => $request->date_db($request->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $request->month_year($request->created_at),
			'time_passed' => $request->time_passed($request->created_at),
			'timestamp' => $request->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeInfo(MarriageCertificate $request){
		$collection = Collection::make([
			'sender'	=> $request->author ? "{$request->author->fname} {$request->author->lname}" : "Anonymous",
			'status'	=> $request->status,
			'code'	=> $request->code,
			'content' => $request->content,
			'geolocation'	=> [
    			"lat"			=> $request->geo_long,
    			"long"			=> $request->geo_lat,
    		],
 			'path' => $request->path,
 			'directory' => $request->directory,
 			'full_path' => $request->path ? "{$request->directory}/resized/{$request->filename}" : asset("{$request->directory}/resized/{$request->filename}"),
 			'thumb_path' => $request->path ? "{$request->directory}/thumbnails/{$request->filename}" : asset("{$request->directory}/thumbnails/{$request->filename}"),
		]);
		return $this->item($collection, new MasterTransformer);
	}

	public function includeUser(MarriageCertificate $request){
       $user = $request->author ? : new User;
       if(is_null($user->id)){ $user->id = 0;}
       return $this->item($user, new UserTransformer);
    }
}