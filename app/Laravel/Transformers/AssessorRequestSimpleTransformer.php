<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\AssessorRequestSimple;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class AssessorRequestSimpleTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'more'
    ];

	public function transform(AssessorRequestSimple $assessor){

	     return [
	     	'id' => $assessor->id,
	     	'doc_inspection_status' => $assessor->doc_inspection_status,
			'doc_evaluation_status' => $assessor->doc_evaluation_status,
			'recommendation_status' => $assessor->recommendation_status,
			'final_approval_status' => $assessor->final_approval_status,
			'logofind_status' => $assessor->logofind_status,
			'assessment_record_status' => $assessor->assessment_record_status,
			'total_duration' => "",
	     ];
	}

	public function includeMore(AssessorRequestSimple $assessor){
        $collection = Collection::make([
        	[
        		'title' => Str::title(str_replace('_', ' ', "doc_inspection")),
        		'code' => 'doc_inspection_status',
        		'duration' => ($assessor->doc_inspection_duration ? Helper::date_db($assessor->doc_inspection_to) : '?') ,
        		'status' => $assessor->doc_inspection_status 
        	],

			[
				'title' => Str::title(str_replace('_', ' ', "doc_evaluation")),
				'code' => 'doc_evaluation_status',
				'duration' => ($assessor->doc_evaluation_duration ? Helper::date_db($assessor->doc_evaluation_to) : '?') ,
				'status' => $assessor->doc_evaluation_status 
			],

			[
				'title' => Str::title(str_replace('_', ' ', "recommendation")),
				'code' => 'recommendation_status',
				'duration' => ($assessor->recommendation_duration ? Helper::date_db($assessor->recommendation_to) : '?') ,
				'status' => $assessor->recommendation_status 
			],

			[
				'title' => Str::title(str_replace('_', ' ', "final_approval")),
				'code' => 'final_approval_status',
				'duration' => ($assessor->final_approval_duration ? Helper::date_db($assessor->final_approval_to) : '?') ,
				'status' => $assessor->final_approval_status 
			],

			[
				'title' => Str::title(str_replace('_', ' ', "logofind")),
				'code' => 'logofind_status',
				'duration' => ($assessor->logofind_duration ? Helper::date_db($assessor->logofind_to) : '?') ,
				'status' => $assessor->logofind_status 
			],

			[
				'title' => Str::title(str_replace('_', ' ', "assessment_record")),
				'code' => 'assessment_record_status',
				'duration' => ($assessor->assessment_record_duration ? Helper::date_db($assessor->assessment_record_to) : '?') ,
				'status' => $assessor->assessment_record_status 
			],

    	]);
        return $this->item($collection, new MasterTransformer);
	}
}