<?php

namespace App\Laravel\Transformers;

use App\Laravel\Models\Poll;

use Illuminate\Support\Collection;
use App\Laravel\Transformers\MasterTransformer;
use League\Fractal\TransformerAbstract;

use DB,Helper,Str,Cache,Carbon,Input;

class PollTransformer extends TransformerAbstract{

	protected $availableIncludes = [
		'date','choices'
    ];

	public function transform(Poll $poll){
	     return [
	     	'id' => $poll->id,
	     	'question' => $poll->question,
	     ];
	}

	public function includeDate(Poll $poll){
        $collection = Collection::make([
			'date_db' => $poll->date_db($poll->created_at,env("MASTER_DB_DRIVER","mysql")),
			'month_year' => $poll->month_year($poll->created_at),
			'time_passed' => $poll->time_passed($poll->created_at),
			'timestamp' => $poll->created_at
    	]);
        return $this->item($collection, new MasterTransformer);
	}

	public function includeChoices(Poll $poll){
		$json_array = json_decode($poll->choices_metadata);

		$choices = [];

		foreach($json_array as $index => $choice){
			array_push($choices,[$index => $choice]);
		}

		$collection = Collection::make($choices);

		return $this->collection($choices, new MasterTransformer);
	}
}