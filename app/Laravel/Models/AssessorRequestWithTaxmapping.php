<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;
use App\Laravel\Traits\StatusTrait;

use Helper, Carbon;

class AssessorRequestWithTaxmapping extends Model{
	
	use SoftDeletes, DateFormatterTrait, StatusTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'assessor_request_with_taxmapping';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = ['citizen_request_code','property_owner','property_location','pin','office_order_preparation_user_id','office_order_preparation_status','office_order_preparation_remarks','office_order_preparation_from','office_order_preparation_to','office_order_preparation_duration','office_order_approval_user_id','office_order_approval_status','office_order_approval_remarks','office_order_approval_from','office_order_approval_to','office_order_approval_duration','doc_inspection_user_id','doc_inspection_status','doc_inspection_remarks','doc_inspection_from','doc_inspection_to','doc_inspection_duration','taxmapping_user_id','taxmapping_status','taxmapping_remarks','taxmapping_from','taxmapping_to','taxmapping_duration','doc_evaluation_user_id','doc_evaluation_status','doc_evaluation_remarks','doc_evaluation_from','doc_evaluation_to','doc_evaluation_duration','recommendation_user_id','recommendation_status','recommendation_remarks','recommendation_from','recommendation_to','recommendation_duration','final_approval_user_id','final_approval_status','final_approval_remarks','final_approval_from','final_approval_to','final_approval_duration','logofind_user_id','logofind_status','logofind_remarks','logofind_from','logofind_to','logofind_duration','assessment_record_user_id','assessment_record_status','assessment_record_remarks','assessment_record_from','assessment_record_to','assessment_record_duration'];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = ['num_completed','num_process','percentage','total_duration'];

	public function getNumCompletedAttribute(){
        $count = 0;
        $cols = ['office_order_preparation','office_order_approval','doc_inspection','taxmapping','doc_evaluation','recommendation','final_approval','logofind','assessment_record'];

        foreach ($cols as $key => $field) {
        	if($this->{$field . '_status'} == "done") ++$count; 
        }
       
        return $count;
    }

    public function getNumProcessAttribute(){
        $cols = ['office_order_preparation','office_order_approval','doc_inspection','taxmapping','doc_evaluation','recommendation','final_approval','logofind','assessment_record'];
        return count($cols);
    }

    public function getPercentageAttribute(){
    	
    	$percentage = 0;

    	if($this->num_process){
    		$percentage = ($this->num_completed/$this->num_process)*100;
    	}
    	return $percentage;
    }

    public function getTotalDurationAttribute(){
        $sum = 0;
        $cols = ['office_order_preparation','office_order_approval','doc_inspection','taxmapping','doc_evaluation','recommendation','final_approval','logofind','assessment_record'];
        foreach ($cols as $key => $field) {
        	$sum +=  $this->{$field . '_duration'};
        }

         if($this->citizen_request AND $this->citizen_request->status == "pending"){
        	$from = $this->created_at;
			$to = Carbon::now();
			$sum = ceil((strtotime($to) - strtotime($from)) / 60);
        }

        return $sum;
    }
	
	private function _admin($foreign_key){
		return $this->belongsTo("App\Laravel\Models\User","{$foreign_key}_user_id",'id');
	}

	public function office_order_preparation_admin() {
		return $this->_admin("office_order_preparation");
	}

	public function office_order_approval_admin() {
		return $this->_admin("office_order_approval");
	}

	public function doc_inspection_admin() {
		return $this->_admin("doc_inspection");
	}

	public function taxmapping_admin() {
		return $this->_admin("taxmapping");
	}

	public function doc_evaluation_admin() {
		return $this->_admin("doc_evaluation");
	}

	public function recommendation_admin() {
		return $this->_admin("recommendation");
	}

	public function final_approval_admin() {
		return $this->_admin("final_approval");
	}

	public function logofind_admin() {
		return $this->_admin("logofind");
	}

	public function assessment_record_admin() {
		return $this->_admin("assessment_record");
	}

	public function citizen_request(){
		return $this->belongsTo("App\Laravel\Models\CitizenRequest",'citizen_request_id','id');
	}
}