<?php 

namespace App\Laravel\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Laravel\Traits\DateFormatterTrait;

class Directory extends Model{
	
	use SoftDeletes, DateFormatterTrait;
	
	/**
	 * Enable soft delete in table
	 * @var boolean
	 */
	protected $softDelete = true;
	
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'directory';

	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [ 'title','status','directory','filename','path' ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = [];

	/**
	 * The attributes that created within the model.
	 *
	 * @var array
	 */
	protected $appends = [];

	public function establishments(){
		return $this->hasMany("App\Laravel\Models\Establishment",'directory_id','id')->published();
	}

	public function scopePublished($query){
		return $query->where('status','published');
	}

	public function scopeDraft($query){
		return $query->where('status','draft');
	}
	
}