<?php 

namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use App\Laravel\Models\UserDevice;

use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;
use FCM;

use App\Laravel\Services\FCM as Firebase;

class ShakeBlast extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(array $form_data)
	{
		$this->shake_id = $form_data['shake_id'];
		$this->data = $form_data['data'];
	}

	public function job(){
		$devices = UserDevice::where('is_login',1)->get();
		$arr = [];
		
		foreach ($devices as $key => $device) {

			// $optionBuiler = new OptionsBuilder();
			// $optionBuiler->setTimeToLive(60*20);

			// $notificationBuilder = new PayloadNotificationBuilder('Shake Now!');
			// $notificationBuilder->setBody('Shake to win amazing prizes.')
			//                     ->setSound('default');

			// $dataBuilder = new PayloadDataBuilder();
			// $dataBuilder->addData(['type' => 'SHAKE_BLAST']);

			// $option = $optionBuiler->build();
			// $notification = $notificationBuilder->build();
			// $data = $dataBuilder->build();

			// $token = $device->reg_id;

			// $downstreamResponse = FCM::sendTo($token, $option, $notification, $data);

			$fcm = new Firebase;
			$fcm_response = $fcm->send($device->reg_id,[
				"data" => [
					'shake_id' => $this->shake_id,
					'type' => "SHAKE_BLAST"
				],
				'notification' => [
					'title' => "Shake Now!",
					'body' => "Shake to win amazing prizes.",
				],
				'priority' => "high",
			]);

			array_push($arr, $fcm_response);
		}

		// dd($arr);
	}

}
