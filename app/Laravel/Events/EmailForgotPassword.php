<?php namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;
use App\Laravel\Models\User;
use Mail,Str,Helper;

class EmailForgotPassword extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct(array $form_data)
	{
		$this->user_id = $form_data['user_id'];
	}

	public function job(){
		$user = User::find($this->user_id);

		$user->validation_token = Helper::validation_token();
		$user->save();

		$data = ["validation_token" => $user->validation_token, "user_id" => $user->id];

		Mail::send('emails.forgot-password', $data, function($message) use ($user){
			$message->from("no-reply@oneserv.ph");
			$message->to($user->email, $user->fname . " " . $user->lname);
		    $message->subject("Cityserv Palawan - Request for resetting password");
		});
	}

}
