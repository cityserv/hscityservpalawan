<?php namespace App\Laravel\Events;

use Illuminate\Queue\SerializesModels;

use App\Laravel\Models\UserDevice;
use PushNotification as PN;

class PushNotification extends Event {

	use SerializesModels;

	/**
	 * Create a new event instance.
	 *
	 * @return void
	 */
	public function __construct($shake_id, $msg, array $data = ['key' => "value"])
	{
		$this->shake_id = $shake_id;
		$this->msg = $msg;
		$this->data = $data;
	}

	public function job(){

		$devices = UserDevice::where('is_login','1')->get();
		foreach($devices as $device){
			$service = $device->device_name == "android" ? "appNameAndroid" : "appNameIOS";

			$app = PN::app($service);

			$new_client = new \Zend\Http\Client(null, array(
				'adapter' => 'Zend\Http\Client\Adapter\Socket',
				'sslverifypeer' => false
				));
			$app->adapter->setHttpClient($new_client);

			$app->to($device->reg_id)->send($this->msg, $this->data);
		}
	}

}
