<?php namespace App\Laravel\Listeners;

use App\Laravel\Events\ShakeBlast;

class ShakeBlastListener{

	public function handle(ShakeBlast $blast){
		$blast->job();
	}
}