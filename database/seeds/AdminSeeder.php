<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\User;

class AdminSeeder extends Seeder{
	public function run(){

		$first_user = User::find(1);


		if($first_user){
			$input = ['username' => "admin", 'email' => "support@highlysucceed.com",'password' => bcrypt('admin'),'fname' => "Master", 'fname' => "Admin","type" => "super_user",'fb_id' => "1414327452115427"];
			$first_user->fill($input);
			$first_user->save();
		}else{
			if(env('DB_CONNECTION','sqlsrv') != "sqlsrv"){
				User::create(
			    	['id'=> 1,'username' => "admin", 'email' => "support@highlysucceed.com",'password' => bcrypt('admin'),'fname' => "Master", 'fname' => "Admin","type" => "super_user",'fb_id' => "1414327452115427"]
			    );
			}else{
				DB::unprepared ('SET IDENTITY_INSERT [user] ON');
				DB::table('user')->insert(['id'=> 1,'username' => "admin", 'email' => "support@highlysucceed.com",'password' => bcrypt('admin'),'fname' => "Master", 'fname' => "Admin","type" => "super_user",'fb_id' => "1414327452115427"]);
				DB::unprepared ('SET IDENTITY_INSERT [user] off');
			}
			    
		}


	   
	}
}