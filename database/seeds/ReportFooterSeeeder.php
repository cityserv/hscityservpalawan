<?php

use Illuminate\Database\Seeder;
use App\Laravel\Models\ReportFooter;

use Carbon\Carbon;
use Illuminate\Support\Str;

class ReportFooterSeeder extends Seeder{
	public function run(){

	   	ReportFooter::truncate();
	   	ReportFooter::create([
	   		'title' => "Scholarship Footer",
	   		'content' => "<div class='row'>
				<div class='col-xs-3'>
					<p class='m-b-20'>CERTIFIED: Services have been duly rendered as stated</p>
					<p class='text-center'>
						<span class='bold signature'>MANOLO M. PERLADA</span><br>
						<small>Administrative Officer V</small>
					</p>
					<p class='m-b-20'>CERTIFIED: funds available in the amount of P _________</p>
					<p class='text-center'>
						<span class='bold signature'>NILA PETRONILA R. OLIVARIO</span><br>
						<small>Assistant City Treasurer</small>
					</p>
				</div>
				<div class='col-xs-4'>
					<p class='m-t-10 m-b-20'>
						CERTIFIED: Completeness &amp; proprierty of supporting document
						previous cash advance liquidated/existence of fund held in trust
					</p>
					<p class='text-center'>
						<span class='bold signature'>ASTER P. MARASIGAN</span><br>
						<small>Asst. City Accountant-OIC</small>
					</p>
				</div>
				<div class='col-xs-3'>
					<p class='m-b-20'>APPROVED FOR PAYMENT:</p>
					<p class='text-center'>
						<span class='bold signature'>BEVERLEY ROSE A. DIMACHUA</span><br>
						<small>Administrative Officer V</small>
					</p>
					<p class='m-b-20'>
						CERTIFIED: Each employee whose name appears above has been paid the
						amount indicated opposite his/her name.
					</p>
					<div class='row'>
						<div class='col-xs-6'>
							<p class='text-center'>
								<span class='bold signature'>Disbursing Officer</span>
							</p>
						</div>
						<div class='col-xs-6'>
							<p class='text-center'>
								<span class='bold signature'>&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;</span>
							</p>
						</div>
					</div>
				</div>
			</div>",
	   	]);
	}
}