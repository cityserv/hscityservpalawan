<?php

use Illuminate\Database\Seeder;

use App\Laravel\Models\CitizenRequest;
use App\Laravel\Models\CitizenRequestLog;
use App\Laravel\Models\CRTracker;
use App\Laravel\Models\CRTrackerHeader;
use App\Laravel\Models\UserLog;
use App\Laravel\Models\UserTracker;

use App\Laravel\Models\Queue;
use App\Laravel\Models\QueueForDisplay;

class SoftReset extends Seeder{
	public function run(){
		CitizenRequest::truncate();
	   	CitizenRequestLog::truncate();
		CRTracker::truncate();
		CRTrackerHeader::truncate();
		Queue::truncate();
		QueueForDisplay::truncate();
		UserLog::truncate();
		UserTracker::truncate();
	}
}