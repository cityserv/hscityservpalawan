<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizenRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizen_request', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('user_id')->default(0);
            $table->string('code');
            $table->string('title');
            
            $table->string('name');
            $table->string('email')->nullable();
            $table->string('contact_number',25)->nullable();

            $table->string('target_table');
            $table->string('type',100);
            $table->string('sub_type',100)->nullable();

            $table->text('remarks')->nullable();
            $table->string('status',50)->default('pending');

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('citizen_request');
    }
}
