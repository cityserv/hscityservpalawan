<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDeathCertificateRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('death_certificate_request', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('user_id')->default(0);
            $table->string('code',20);
            $table->string('req_address',100);
            $table->string('contact',13);
            $table->string('lname',50);
            $table->string('fname',50);
            $table->string('middle_name',50);

            $table->text('place_of_death');
            $table->date('date_of_death');

            $table->text('purpose');
            $table->string('number_of_copies',2)->default("1");

            $table->string('status',50)->default("pending");
            $table->dateTime('posted_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('death_certificate_request');
    }
}
