<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForDisplayQueueTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('queue_for_display', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('queue_id')->default(0);
            $table->string('service_type',50);
            $table->string('queue_no',60);
            $table->string('status',50)->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('queue_for_display');
    }
}
