<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePollsTableAddedFieldsForImages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('polls', function($table)
        {
            $table->text('directory')->nullable();
            $table->string('filename',150)->nullable();
            $table->text('path')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('polls', function($table)
        {
            $table->dropColumn(array('directory'));
            $table->dropColumn(array('filename'));
            $table->dropColumn(array('path'));
        });
    }
}
