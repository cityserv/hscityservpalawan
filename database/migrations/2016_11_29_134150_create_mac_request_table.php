<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMacRequestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('mac_request', function(Blueprint $table)
        {
            $table->increments('id');
            $table->string('citizen_request_id');

            $table->date('schedule_appointment')->nullable();
            $table->date('final_interview')->nullable();
            
            $table->integer('info_validation_user_id')->default(0);
            $table->string('info_validation_status',50)->default('pending');
            $table->text('info_validation_remarks')->nullable();
            $table->dateTime('info_validation_from');
            $table->dateTime('info_validation_to');
            $table->integer('info_validation_duration')->nullable();

            $table->integer('assessment_evaluation_user_id')->default(0);
            $table->string('assessment_evaluation_status',50)->default('pending');
            $table->text('assessment_evaluation_remarks')->nullable();
            $table->dateTime('assessment_evaluation_from');
            $table->dateTime('assessment_evaluation_to');
            $table->integer('assessment_evaluation_duration')->nullable();

            $table->integer('final_interview_user_id')->default(0);
            $table->string('final_interview_status',50)->default('pending');
            $table->text('final_interview_remarks')->nullable();
            $table->dateTime('final_interview_from');
            $table->dateTime('final_interview_to');
            $table->integer('final_interview_duration')->nullable();

            $table->integer('requirement_submission_user_id')->default(0);
            $table->string('requirement_submission_status',50)->default('pending');
            $table->text('requirement_submission_remarks')->nullable();
            $table->dateTime('requirement_submission_from');
            $table->dateTime('requirement_submission_to');
            $table->integer('requirement_submission_duration')->nullable();

            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('mac_request');
    }
}
