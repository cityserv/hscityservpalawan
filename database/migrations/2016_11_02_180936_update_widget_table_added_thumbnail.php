<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateWidgetTableAddedThumbnail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('widget', function($table)
        {
            $table->text('directory')->nullable()->after('code');
            $table->string('filename',150)->nullable()->after('code');
            $table->text('path')->nullable()->after('code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('widget', function($table)
        {
            $table->dropColumn(array('directory','filename','path'));
        });
    }
}
