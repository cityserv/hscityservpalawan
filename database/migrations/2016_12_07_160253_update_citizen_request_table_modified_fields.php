<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateCitizenRequestTableModifiedFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('citizen_request', function($table)
        {
            $table->string('category')->nullable()->after('sub_type');
            $table->string('subcategory')->nullable()->after('sub_type');
            $table->string('module')->nullable()->after('sub_type');
            $table->string('beneficiary_rel_to_client',50)->nullable()->after('contact_number');
            $table->decimal('beneficiary_income',25,2)->nullable()->after('contact_number');
            $table->string('beneficiary_occupation',100)->nullable()->after('contact_number');
            $table->string('beneficiary_education',20)->nullable()->after('contact_number');
            $table->text('beneficiary_address')->nullable()->after('contact_number');
            $table->string('beneficiary_religion',50)->nullable()->after('contact_number');
            $table->string('beneficiary_civil_status',20)->nullable()->after('contact_number');
            $table->integer('beneficiary_age')->nullable()->after('contact_number');
            $table->string('beneficiary_birthplace')->nullable()->after('contact_number');
            $table->date('beneficiary_birthdate')->nullable()->after('contact_number');
            $table->string('beneficiary_gender')->nullable()->after('contact_number');
            $table->string('beneficiary_name')->after('contact_number');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('citizen_request', function($table)
        {
            $table->dropColumn(array('category','subcategory','module','beneficiary_rel_to_client','beneficiary_income','beneficiary_occupation','beneficiary_education','beneficiary_address','beneficiary_religion','beneficiary_civil_status','beneficiary_age','beneficiary_birthplace','beneficiary_birthdate','beneficiary_gender','beneficiary_name'));
        });
    }
}
