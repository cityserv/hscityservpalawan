<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAfterAdoptionRequest extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('after_adoption_request', function(Blueprint $table)
        {
            $table->increments('id');
            $table->bigInteger('user_id')->default(0);
            $table->string('code',20);
            $table->string('req_address',100);
            $table->string('contact',13);
            $table->string('lname',50);
            $table->string('fname',50);
            $table->string('middle_name',50);
            $table->string('father_lname',50);
            $table->string('father_fname',50);
            $table->string('father_middle_name',50);
            $table->string('father_occupation',100);
            $table->string('father_religion',50);
            $table->string('father_age',2);
            $table->string('mother_lname',50);
            $table->string('mother_fname',50);
            $table->string('mother_middle_name',50);
            $table->string('mother_occupation',100);
            $table->string('mother_religion',50);
            $table->string('mother_age',2);

            $table->date('decree_issued');
            $table->date('final_decree');

            $table->string('court_name',50);
            $table->string('judge_lname',50);
            $table->string('judge_fname',50);
            $table->string('judge_middle_name',50);

            // $table->text('place_of_birth');
            // $table->date('date_of_birth');

            $table->text('purpose');
            $table->string('number_of_copies',2)->default("1");

            $table->string('status',50)->default("pending");
            $table->dateTime('posted_at');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('after_adoption_request');
    }
}
