<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_service', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('service_id')->default(0);

            $table->string('code');

            $table->string('title',150);
            $table->text('content')->nullable();

            $table->integer('order')->default(0);
            $table->enum('status',["draft","published"])->default("draft");
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sub_service');
    }
}
