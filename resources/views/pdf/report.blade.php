<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<title></title>
	<style type="text/css">
		body{font-family:Arial,Helvetica Neue,Helvetica,sans-serif;padding:0;margin:0}.content{padding:5px;margin:5px}.text-center{text-align:center}.text-right{text-align:right}.bold{font-weight:bold}.table{width:100%;border-spacing:0;border-collapse:collapse;font-size:12px}.table table,.table th,.table td{border:1px solid black}.table th{padding:7px}.m-t-0{margin-top:0}.m-b-0{margin-bottom:0}.p-b-0{padding-bottom:0}.m-t-10{margin-top:10}.m-b-20{margin-bottom:20}.row{width:100%;display:table}.col-xs-3{width:30%;display:table-cell}.col-xs-4{width:40%;display:table-cell}.col-xs-6{width:50%;display:table-cell}.footer{page-break-inside:avoid;font-size:10px}.signature{border-top:1px solid black}
	</style>
</head>
<body>
	<div class="content">
		<div class="heading text-center">
			<p>{!!$report_header!!}</p>
		</div>
		<div class="body m-b-20">
			<p class="bold text-center m-b-0">
				{{$report_title}}
			</p>
			<table class="table">
				<thead>
					<tr>
						<th width="5%"></th>
						<th width="20%" class="text-center">NAME</th>
						<th width="15%" class="text-center">ACCOUNT NO.</th>
						<th width="15%" class="text-center"></th>
						<th width="10%" class="text-center"></th>
						<th width="5%" class="text-center"></th>
						<th width="20%" class="text-center">SIGNATURE</th>
					</tr>
				</thead>
				<tbody>
					@if($requests->count() > 0)
					@foreach($requests as $index => $request)
					<tr>
						<td class="text-center">{{ $index + 1 }}</td>
						<td>{{$request->beneficiary_name}}</td>
						<td>{{$request->author ? $request->author->account_number : "---"}}</td>
						<td>{{$subcategory->code == "scholarship" ? "Subsidy Allowance" : $subcategory->title}}</td>
						<td class="text-right">{{ number_format($request->tracker ? $request->tracker->release_amount : 0, 2) }}</td>
						<td class="text-center">{{ $index + 1 }}</td>
						<td></td>
					</tr>
					@endforeach
					<tr>
						<td></td>
						<td ></td>
						<td ></td>
						<td class="bold text-right">Total</td>
						<td class="bold text-right">PHP {{ number_format($total, 2) }}</td>
						<td ></td>
						<td></td>
					</tr>
					@else
					<tr>
						<td colspan="7" class="text-center">No data available.</td>
					</tr>
					@endif
			</table>
		</div>
		<div class="footer">
			{!! $report_footer !!}
		</div>
	</div>
</body>
</html>