@extends('backoffice._template.auth')
@section('content')
	
<!-- Advanced login -->
<form action="" id="target" method="POST">

	@if(Session::has('notification-status'))
	<div class="login-form">
		@include('backoffice._includes.page-alert')
	</div>
	@endif

	<div class="panel panel-body login-form">

		<input type="hidden" name="_token" value={{csrf_token()}}>
		<div class="thumb thumb-rounded">
			@if($auth->filename)
			<img src="{{asset($auth->directory . '/' . $auth->filename)}}" alt="">
			@else
			<img src="{{asset('backoffice/images/demo/users/face0.jpg')}}" alt="">
			@endif
			<div class="caption-overflow"></div>
		</div>

		<div class="text-center">
			<h5 class="content-group-lg">{{$auth->fname . " " . $auth->lname}} <small class="display-block">Unlock your account</small></h5>
		</div>

		<div class="form-group has-feedback has-feedback-left">
			<input type="password" class="form-control" name="password" placeholder="Password" required>
			<div class="form-control-feedback">
				<i class="icon-lock2 text-muted"></i>
			</div>
		</div>

		<div class="form-group login-options">
			<div class="row">
				<div class="col-md-12 text-right">
					<label class="checkbox-inline">
						<input type="checkbox" name="remember_me" class="styled" checked="checked">
						Remember me
					</label>
				</div>

				<!-- <div class="col-sm-6 text-right">
					<a href="#">Forgot password?</a>
				</div> -->
			</div>
		</div>

		<div class="form-group text-center">
			<button id="unlock" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Unlocking ..." class="btn btn-primary btn-lg btn-raised btn-loading">unlock</button>
			<a id="logout" href="{{route('backoffice.logout')}}" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Bye-bye ..." class="btn btn-default btn-lg btn-raised btn-loading">Logout</a>
		</div>
	</div>
</form>
@stop
@section('page-styles')
@stop
@section('page-scripts')

<script type="text/javascript">
	$(function(){
		$('#target').submit(function (event) {
	        $('#unlock').button('loading');
	    });

	    $('#logout').click(function(){
	    	$(this).button('loading');
	    });
	});
</script>
@stop