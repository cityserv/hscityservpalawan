@extends("backoffice._template.queue")

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-8">
			<div class="category-header">
				<h4 class="content-group text-semibold">Create a Request <small class="display-block">Choose a service to create generate new number</small></h4>
			</div>
			<div class="category-content">
				<div class="row">
					<div class="col-xs-4">
						<button class="btn btn-service bg-pink-300 btn-block btn-float btn-float-lg legitRipple" data-service_type="social_service" type="button"><i class="icon-accessibility2"></i> <span>Social Service</span></button>
						<button class="btn btn-service bg-purple-300 btn-block btn-float btn-float-lg legitRipple" type="button" data-service_type="health_service"><i class="icon-pulse2"></i> <span>Health Service</span></button>
					</div>

					<div class="col-xs-4">
						<button class="btn btn-service bg-warning-300 btn-block btn-float btn-float-lg legitRipple" type="button" data-service_type="scholarship_program"><i class="icon-books"></i> <span>Scholarship</span></button>
						<button class="btn btn-service bg-brown-300 btn-block btn-float btn-float-lg legitRipple" type="button" data-service_type="legal_service"><i class="icon-balance"></i> <span>Legal Service</span></button>
					</div>
					<div class="col-xs-4">
						<button class="btn btn-service bg-slate-300 btn-block btn-float btn-float-lg legitRipple" type="button" data-service_type="administrative"><i class="icon-people"></i> <span>Administrative</span></button>
					</div>
				</div>
			</div>
			<div class="citizen-header">
				<h4 class="content-group text-semibold">Citizen Accounts <small class="display-block">Manage citizen accounts</small></h4>
			</div>
			<div class="citizen-content" style="padding: 20px">
				<div class="row">
					<div class="col-xs-4">
						<a class="btn btn-primary btn-block btn-float btn-float-lg legitRipple" target="_blank" href="{{route('backoffice.users.index')}}""><i class="icon-users"></i><span>View Citizen Accounts</span></a>
					</div>
					<div class="col-xs-4">
						<a class="btn btn-primary btn-block btn-float btn-float-lg legitRipple" target="_blank" href="{{route('backoffice.users.create')}}"><i class="icon-plus3"></i> <span>Create New Citizen Account</span></a>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-flat">
				<div class="panel-heading">
					<h6 class="panel-title">Recently Queued</h6>
				<a class="heading-elements-toggle"><i class="icon-more"></i></a></div>

				<div class="panel-body">
					<ul id="queue_list" class="media-list media-list-bordered">
						@forelse($recent_queue as $index => $request)
						<li class="media">
							<div class="media-body">
								<div class="media-heading">
									<strong style="font-size: 18px;">{{$request->queue_no}}</strong> <span><i>- {{$request->name}}</i></span>
								</div>
								<span>{{Helper::get_service_display($request->service_type)}}</span>
								@if($request->is_priority == "yes")
								<small class="label label-rounded border-danger text-danger-600">PRIORITY</small>
								@endif
							</div>
							<div class="media-right">
								<a href="#">
									<span class="label label-flat label-rounded label-icon {{Helper::get_service_display($request->service_type,"icon_color")}}"><i class="{{Helper::get_service_display($request->service_type,"icon")}}"></i></span>
								</a>
							</div>
						</li>
						@empty
						<li class="media">
							<div class="media-body">
								<h6><i>List is empty.</i></h6>
							</div>
						</li>
						@endforelse
					</ul>
				</div>
				<div class="panel-footer">
					<div class="heading-elements">
						<span class="heading-text text-semibold">Overall Total:</span>
						<span class="heading-text pull-right"><span id="total_counter" class="badge bg-success-400 position-left">{{$total_queue}}</span></span>
					</div>
					@if($most_queue)
					<div class="heading-elements">
						<span class="heading-text text-semibold">Most Requested : <small id="most_queue_title">{{Helper::get_service_display($most_queue->service_type)}}</small></span>
						<span class="heading-text pull-right"><span id="most_queue_counter" class="badge bg-success-400 position-left">{{$most_queue->sum_queue}}</span></span>
					</div>
					@endif
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section('page-scripts')
<script type="text/javascript" src="{{asset('backoffice/js/plugins/notifications/sweet_alert.min.js')}}"></script>
<script type="text/javascript">
	var _master_token = "{{env("APP_KEY")}}";
	var _total_queue = "{{$total_queue}}";
	@if($most_queue)
	var _most_queue_title = "{{$most_queue->service_type}}";
	var _most_queue_counter = "{{$most_queue->sum_queue}}";
	@else
	var _most_queue_title = "";
	var _most_queue_counter = "0";
	@endif

	$(function(){
		$("body").delegate(".btn-request","click",function(e){
			var _index = $(this).index();
			var _sibling = $(this).parent().siblings().children('a');
			_sibling.removeClass("btn-success");
			$(this).addClass("btn-success");

			return false;
		});

		$(".btn-service").on("click",function(e){
			var _btn = $(this);
			var _type = _btn.data("service_type")
			swal({
			  title: "Create Request for "+$(this).get_service(_type,"title"),
			  text: '<p>Service Type</p><fieldset>'+
			  	'<div class="btn-group"  aria-label="Basic example">'+
			    '<span class="col-xs-6"><a href="#" class="btn btn-request btn-xs btn-success" data-value="normal">Regular Request</a></span>'+
			    '<span class="col-xs-6"><a href="#" class="btn btn-request btn-xs" data-value="priority">Priority Request</a></span>'+
			  '</div>'+
			  '</fieldset><br>Name of citizen',
			  type: "input",
			  html: true,
			  animation: "slide-from-top",
			  inputPlaceholder: "Please indicate citizen name",
		      showCancelButton: true,
		      closeOnConfirm: false,
		      confirmButtonColor: "#4caf50",
		      confirmButtonText : "Create and Print",
		      showLoaderOnConfirm: true
			},
			function(citizen_name){
			  if (citizen_name === false) return false;
			  
			  if (citizen_name === "") {
			    swal.showInputError("You need to write something!");
			    return false
			  }

			  var _request = $(".btn-request.btn-success").data("value");
			  var is_priority = "no";
			  if(_request == "priority"){
			  	is_priority = "yes";
			  }

			  $.ajax({
			  	data : {api_token : _master_token, service_type : _type, name : citizen_name, is_priority : is_priority},
			  	url : "{{URL::to('api/queue/create.json')}}",
			  	type :"POST",
			  	dataType : "json",
			  	success : function(data){
			  		console.log(data);

			  		swal({
			  			title : "Success!",
			  			type : "success",
			  			text : data.msg,
			  			timer: 2000,
			  			showConfirmButton: true
			  		},function(){
			  			// window.location.reload();

			  			window.open("{{route('backoffice.queue.ticket',[""])}}/"+data.data.queue_no, '_blank');
	  			  	})
			  	},
			  	error: function(jqXHR,textStatus,thrownError){
			  		console.log(jqXHR);
			  	}
			  });
			});

		});
		setInterval(get_data, 4000);
        function get_data(){
             $.ajax("{{URL::to('api/queue/updated-data.json')}}", {
                type: 'POST',
                data : { api_token : _master_token},
                async: true,
                success: function(data) {
                	console.log(data);
                    var total_queue = data.total_queue;
                    var most_queue_title = data.most_queue.service_type;
                    var most_queue_counter = data.most_queue.sum_queue;
                    if(total_queue != _total_queue){
                    	_total_queue = total_queue;

                    	$("#total_counter").fadeOut(300,function(){
                    		$(this).text(total_queue)
                    		$(this).fadeIn(300);
                    	})

        	       		var recent_queue = data.recent_queue;

        	       		$("#queue_list").fadeOut(500,function(){
        	       			$(this).empty();
        	       			$("#queue_list").fadeIn(200,function(){
	   				        	$.each(recent_queue,function(index,queue){
	   				        		var priority_content = "";
	   				        		if(queue.is_priority == "yes"){
	   				        			priority_content = '<small class="label label-rounded border-danger text-danger-600">PRIORITY</small>';
	   				        		}

	   				        		$('#queue_list').append('<li class="media">'+
	   				        			'<div class="media-body">'+
	   				        				'<div class="media-heading">'+
	   				        					'<strong style="font-size: 18px;">'+queue.queue_no+'</strong> <span><i>- '+queue.name+'</i></span>'+
	   				        					'</div>'+
	   				        					'<span>'+queue.for_display+'</span> '+
	   				        					priority_content+
	   				        					'</div>'+
	   										'<div class="media-right">'+
	   											'<a href="#">'+
	   												'<span class="label label-flat label-rounded label-icon '+queue.icon_color+'"><i class="'+queue.icon+'"></i></span>'+
	   											'</a>'+
	   										'</div>'+
	   									'</li>').hide()
	   				        		    .fadeIn(300);
	   				        	});
        	       			});
        	       		});

        	        	
                    }
                    if(most_queue_title != _most_queue_title){
                    	_most_queue_title = most_queue_title;

                    	$("#most_queue_title").fadeOut(300,function(){
                    		$(this).text($(this).get_service(most_queue_title,"title"))
                    		$(this).fadeIn(300);
                    	})
                   		
                    }
                    if(most_queue_counter != _most_queue_counter){
                    	_most_queue_counter = most_queue_counter;

                    	$("#most_queue_counter").fadeOut(300,function(){
                    		$(this).text(most_queue_counter)
                    		$(this).fadeIn(300);
                    	})
                    }
                },
                error : function(jqXHR,textStatus,thrownError){
                    // alert("Error")
                }
            });

        }

	});


	

</script>
@stop
