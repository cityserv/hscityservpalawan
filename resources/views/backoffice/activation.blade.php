@extends("backoffice._template.blank")

@section('content')
<div class="content mt-20">
	<div class="panel">
		<div class="panel-heading">
			<h3 class="text-semibold text-center">Cityserv Batangas - Account Activation</h3>
		</div>
		<div class="panel-body">
			<h4 class="text-center text-light text-semibold">{!! $message !!}</h4>
		</div>
		<div class="panel-footer">
			<p class="text-center">&copy; {{ Carbon::now()->format("Y") }} Cityserv Batangas</p>
		</div>
	</div>
</div>
@stop

@section('page-scripts')
@stop