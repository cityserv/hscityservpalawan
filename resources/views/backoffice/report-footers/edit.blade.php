@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-stack2"></i> <span class="text-semibold">Report Footers</span> - Edit a report footer.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.report_footers.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
				<a href="{{route('backoffice.report_footers.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Create New</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.report_footers.index')}}"> Report Footers</a></li>
			<li class="active">Create</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Footer Details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<p class="content-group-lg">Below are the general information for this report footer.</p>
				
				<div class="form-group {{$errors->first('title') ? 'has-error' : NULL}}">
					<label for="title" class="control-label col-lg-2 text-right">Title <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="title" id="title" placeholder="" maxlength="100" value="{{old('title',$footer->title)}}">
						@if($errors->first('title'))
						<span class="help-block">{{$errors->first('title')}}</span>
						@endif
					</div>
				</div>
				
			</div>
		</div>

		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Footer Content</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				<p class="content-group-lg">The content of your footer.</p>
				<textarea type="textbox" name="content" id="content" class="summernote">{!!old('content',$footer->content)!!}</textarea>
				<span class="help-block">The content in this field might appear differently when displayed in the generated report.</span>
			</div>
		</div>

		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.report_footers.index')}}">Cancel</a>
			</div>
		</div>
	</form>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<!-- Summernote -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/summernote/summernote.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>


<script type="text/javascript">
	$(function(){

		$('#content').summernote({
	    	height: 400,
	    	onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
	    });

	     function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            data.append("api_token","{{env('APP_KEY')}}");
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('api.summernote',['json'])}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                	if(data.status == true){
                		$('#content').summernote('insertImage', data.image);
                	}
                }
            });
        }

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	    $('.daterange-single').daterangepicker({ 
			singleDatePicker: true,
			timePicker: true,
			timePickerIncrement: 1,
			locale: {
				format: 'YYYY-MM-DD h:mm A'
			}
		});

	});
</script>
@stop