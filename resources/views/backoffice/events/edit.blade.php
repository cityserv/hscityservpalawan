@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-calendar"></i> <span class="text-semibold">Events</span> - Edit an event.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.events.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
				<a href="{{route('backoffice.events.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Create New</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.events.index')}}"> Events</a></li>
			<li class="active">Create</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Event Details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<p class="content-group-lg">Below are the general information for this event.</p>
				
				<div class="form-group {{$errors->first('title') ? 'has-error' : NULL}}">
					<label for="title" class="control-label col-lg-2 text-right">Title <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="title" id="title" placeholder="" maxlength="100" value="{{old('title',$event->title)}}">
						@if($errors->first('title'))
						<span class="help-block">{{$errors->first('title')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('venue') ? 'has-error' : NULL}}">
					<label for="venue" class="control-label col-lg-2 text-right">Venue</label>
					<div class="col-lg-9">
						<input class="form-control" type="text" name="venue" id="venue" placeholder="" maxlength="255" value="{{old('venue',$event->venue)}}">
						@if($errors->first('venue'))
						<span class="help-block">{{$errors->first('venue')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('content') ? 'has-error' : NULL}}">
					<label for="content" class="control-label col-lg-2 text-right">Content</label>
					<div class="col-lg-9">
						<textarea name="content" id="content" cols="5" rows="5" class="form-control summernote" placeholder="Type anything here...">{!!old('content',$event->content)!!}</textarea>
						@if($errors->first('content'))
						<span class="help-block">{{$errors->first('content')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('posted_at') ? 'has-error' : NULL}}">
					<label for="posted_at" class="control-label col-lg-2 text-right">Posting Date <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<div class="input-group">
							<span class="input-group-addon"><i class="icon-calendar22"></i></span>
							<input type="text" name="posted_at" class="form-control daterange-single" value="{{old('posted_at',$event->posted_at)}}">
						</div>
						<span class="help-block">The date and time that this event will be displayed.</span>
						@if($errors->first('posted_at'))
						<span class="help-block">{{$errors->first('posted_at')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('file') ? 'has-error' : NULL}}">
					<label class="control-label col-lg-2 text-right">Upload thumbnail</label>
					<div class="col-lg-9">
						<input type="file" name="file" class="file-styled-primary">
						@if($errors->first('file'))
						<span class="help-block">{{$errors->first('file')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group">
					@if($event->filename)
					<label class="control-label col-lg-2 text-right">Current thumbnail</label>
					<div class="col-lg-9">
						<img src="{{asset($event->directory.'/resized/'.$event->filename)}}" class="img-responsive img-rounded" width="250" alt="">
					</div>
					@else
					<div class="col-lg-offset-2">
						<label for="" class="pl-10">No thumbnail yet.</label>
					</div>
					@endif
				</div>

				<div class="form-group {{$errors->first('status') ? 'has-error' : NULL}}">
					<label for="status" class="control-label col-lg-2 text-right">Status <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("status", $statuses, old('status',$event->status), ['id' => "status", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('status'))
						<span class="help-block">{{$errors->first('status')}}</span>
						@endif
					</div>
				</div>
				
			</div>
		</div>
		
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Geo Location</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
                <div class="col-lg-12 pr-20 pl-20">
                    <div id="post-property-map-container" class="fa fa-search form-control">
                        <input type="text" name="search" id="post-property-map" placeholder="Find your property here" style="width: 90%; border: 0;">
                    </div>
                    <div id="select-property-location" style="height: 350px;"></div>
                    {!!Form::hidden('geo_lat','',array('id' => "map_lat",'value'=> old('geo_lat',$geo_lat)))!!}
                    {!!Form::hidden('geo_long','',array('id' => "map_long",'value'=> old('geo_long',$geo_long)))!!}
                    <span class="help-block">You may search in the address bar or drag the marker to your desired location</span>
                </div>
	                   
			</div>
		</div>

		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.events.index')}}">Cancel</a>
			</div>
		</div>
	</form>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/summernote/summernote.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Datepicker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<!-- GMAP -->
<script src="{{asset('backoffice/js/plugins/locationpicker/locationpicker.jquery.js')}}"></script>
<script src="http://maps.google.com/maps/api/js?sensor=false&libraries=places&key={{env('GOOGLE_MAP_KEY')}}"></script>

<!-- CKEditor -->
{{--<script type="text/javascript" src="{{asset('backoffice/js/plugins/editors/ckeditor/ckeditor.js')}}"></script>--}}

<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	    $('.daterange-single').daterangepicker({ 
	        singleDatePicker: true,
	        timePicker: true,
	        timePickerIncrement: 5,
	         locale: {
	            format: 'YYYY-MM-DD h:mm A'
	        }
	    });

		$("#select-property-location").locationpicker({
            location: {latitude: "{{old('geo_lat',$geo_lat)}}", longitude: "{{old('geo_long',$geo_long)}}"},
            zoom : 14,
            maxZoom : 18,
            minZoom : 14,
            radius: 20,
            inputBinding: {
                locationNameInput: $('#post-property-map')        
            },
            enableAutocomplete: true,
            onchanged: function(currentLocation, radius, isMarkerDropped) {
                var addressComponents = $(this).locationpicker('map').location.addressComponents;
                var loc =$(this).locationpicker('map').location;
                $("#map_lat").val(currentLocation.latitude);
                $("#map_long").val(currentLocation.longitude);
                $("#exact_address").val(loc.formattedAddress);
            }   
        });  

	});
</script>
<script type="text/javascript">
	$(function(){
	    $('#content').summernote({
	    	height: 400,
	    	onImageUpload: function(files, editor, welEditable) {
                sendFile(files[0], editor, welEditable);
            }
	    });

	     function sendFile(file, editor, welEditable) {
            data = new FormData();
            data.append("file", file);
            data.append("api_token","{{env('APP_TOKEN','base64:Lhp1BB3ms7WVgXBKbOkSmDQaIYlCQu/sXfV1Tp2woR0=')}}");
            $.ajax({
                data: data,
                type: "POST",
                url: "{{route('api.summernote',['json'])}}",
                cache: false,
                contentType: false,
                processData: false,
                success: function(data) {
                	if(data.status == true){
                		$('#content').summernote('insertImage', data.image);
                	}
                }
            });
        }

	});
</script>
@stop