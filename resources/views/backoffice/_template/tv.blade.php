<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=UTF-8" />
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>{{env('APP_TITLE',"Localhost")}}</title>

	<!-- Global stylesheets -->
	<link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/icons/fontawesome/styles.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/core.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/components.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('backoffice/css/colors.css')}}" rel="stylesheet" type="text/css">
	<!-- /global stylesheets -->

	<!-- Core JS files -->
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/loaders/pace.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/core/libraries/jquery.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/core/libraries/bootstrap.min.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/loaders/blockui.min.js')}}"></script>
	<!-- /core JS files -->

	<style type="text/css">
		.navbar-header {
		    min-width: 192px;
		}
		.navbar-brand>img {
		    margin-top: 2px;
		    height: 20px;
		}
		.bg-pepero {
			background-color: rgb(10, 127, 49);
		    border-color: rgb(10, 127, 49);
		    color: #fff;
		}
		
		.page-container{
			 background-image: url('{{asset('backoffice/images/tv.jpg')}}');
			 background-attachment: fixed;
			 background-size: cover;
			 background-position: center;
		}

	</style>
	@yield('page-styles')

	<!-- Theme JS files -->
	@yield('page-scripts')
	<script type="text/javascript" src="{{asset('backoffice/js/core/app.js')}}"></script>
	<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/ripple.min.js')}}"></script>
	<script type="text/javascript">
		$(function(){
			$("[rel=tooltip]").tooltip();
		});
	</script>
</head>

<body >

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main content -->
			<div class="content-wrapper">

				@yield('content')

				@yield('modals')
			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->
	<script type="text/javascript">
		$.fn.get_style = function(code,return_key){
			var result = "";
			var table_heading = "";
			var btn_style = "";
			switch(code){
					case 'health_service':
							btn_style = "btn btn-service bg-purple btn-block btn-float btn-float-lg legitRipple";
							table_heading = "bg-purple";
					break;

					case 'scholarship_program':
							btn_style = "btn btn-service bg-warning btn-block btn-float btn-float-lg legitRipple";
							table_heading = "bg-warning";
					break;

					case 'social_service':
							btn_style = "btn btn-service bg-pink btn-block btn-float btn-float-lg legitRipple";
							table_heading = "bg-pink";
					break;

					case 'legal_service':
							btn_style = "btn btn-service bg-brown btn-block btn-float btn-float-lg legitRipple";
							table_heading = "bg-brown";
					break;

					case 'administrative':
							btn_style = "btn btn-service bg-slate btn-block btn-float btn-float-lg legitRipple";
							table_heading = "bg-slate";
					break;

					case 'priority_lane':
							btn_style = "btn btn-service bg-success btn-block btn-float btn-float-lg legitRipple";
							table_heading = "bg-success";
					break;

					default :
						btn_style = "btn btn-service bg-default btn-block btn-float btn-float-lg legitRipple";
						table_heading = "bg-default";
			}

			switch(return_key){
				case 'table':
					result = table_heading;
				break;

				default:
					result = btn_style;
			}

			return result;
		};
		$.fn.get_service = function(code,return_key){
			var result = "";
			var title = "";
			var _class = "";
			var _icon = "";
			var _icon_color = "";

			switch(code){
				case 'health_service':
					title = "Health Service";
					// _class =
					_icon = "icon-pulse2";
					_icon_color = "border-purple text-purple-300";
				break;
				case 'scholarship_program':
					title = "Scholarship Program";
					_icon = "icon-books";
					_icon_color = "border-warning text-warning-300";
				break;
				case 'social_service':
					title = "Social Service";
					_icon = "icon-accessibility2";
					_icon_color = "border-pink text-pink-300";
				break;
				case 'legal_service':
					title = "Legal Service";
					_icon = "icon-balance";
					_icon_color = "border-brown text-brown-300";
				break;
				case 'administrative':
					title = "Administrative";
					_icon = "icon-people";
					_icon_color = "border-slate text-slate-300";
				break;
				case 'priority_lane':

				break;

			}

			switch(return_key){
				case 'icon':
					result = _icon;
				break;

				case 'icon_color':
					result = _icon_color;
				break;

				case 'class':
					result = _class;
				break;
				default :

				result = title;
			}

			return result;
		}
	</script>
</body>
</html>
