@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-certificate"></i> <span class="text-semibold">Reports</span> - Generate reports.</h4>
		</div>
		<div class="heading-elements">
			{{--
			<div class="heading-btn-group">
				<a href="{{route('backoffice.reports.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Generate</span></a>
			</div>
			--}}
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Reports</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

	<form id="target" class="form-horizontal" action="{{route('backoffice.reports.generate')}}" method="GET" enctype="multipart/form-data">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">Filter</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<div class="form-group {{$errors->first('subcategory') ? 'has-error' : NULL}}">
					<label for="subcategory" class="control-label col-lg-2 text-right">Type <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("subcategory", $subcategories, old('subcategory', $subcategory), ['id' => "subcategory", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('subcategory'))
						<span class="help-block err-msg">{{$errors->first('subcategory')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('status') ? 'has-error' : NULL}}">
					<label for="status" class="control-label col-lg-2 text-right">Status <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("status", $statuses, old('status', $status), ['id' => "status", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('status'))
						<span class="help-block err-msg">{{$errors->first('status')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('school_id') ? 'has-error' : NULL}}">
					<label for="school_id" class="control-label col-lg-2 text-right">School / University </label>
					<div class="col-lg-9">
						{!!Form::select("school_id", $schools, old('school_id', $school_id), ['id' => "school_id", 'class' => "select select-with-search col-xs-12 col-sm-12 col-md-12 col-lg-12"])!!}
						<span class="help-block">If MAC category is Scholarship, please choose one from this list, otherwise leave this field blank.</span>
						@if($errors->first('school_id'))
						<span class="help-block err-msg">{{$errors->first('school_id')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('report_footer_id') ? 'has-error' : NULL}}">
					<label for="report_footer_id" class="control-label col-lg-2 text-right">Footer</label>
					<div class="col-lg-9">
						{!!Form::select("report_footer_id", $report_footers, old('report_footer_id', $report_footer_id), ['id' => "report_footer_id", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('report_footer_id'))
						<span class="help-block err-msg">{{$errors->first('report_footer_id')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('date_range') ? 'has-error' : NULL}}">
					<label for="date_range" class="control-label col-lg-2 text-right">Date Range</label>
					<div class="col-lg-9">
						<input type="text" id="date_range" name="date_range" class="form-control daterange-basic" placeholder="YYYY-MM-DD" value="{{old('date_range', $date_range)}}">
						@if($errors->first('date_range'))
						<span class="help-block err-msg">{{$errors->first('date_range')}}</span>
						@endif
					</div>
				</div>
				
			</div>
		</div>

		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Generating ..." class="btn btn-primary btn-raised btn-lg btn-loading">Generate</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.services.index')}}">Cancel</a>
			</div>
		</div>
	</form>

	@include('backoffice._includes.footer')

</div>
<!-- /content area -->
@stop
@section('modals')
<!-- Basic modal -->
<div id="confirm-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-warning alert-styled-left text-default content-group">
	                <span class="text-semibold">Warning!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Deleting Record...</h6>
				<p>You are about to delete a record, this action can no longer be undone, are you sure you want to proceed?</p>

				<hr>

				<h6 class="text-semibold">What is this message?</h6>
				<p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				<a href="" type="button" class="btn btn-danger" id="btn-confirm-delete">Delete</a>
			</div>
		</div>
	</div>
</div>


<div id="scholaship-footer" style="display: none;">
	<div class="row">
		<div class="col-xs-3">
			<p class="m-b-20">CERTIFIED: Services have been duly rendered as stated</p>
			<p class="text-center">
				<span class="bold signature">MANOLO M. PERLADA</span><br>
				<small>Administrative Officer V</small>
			</p>
			<p class="m-b-20">CERTIFIED: funds available in the amount of P _________</p>
			<p class="text-center">
				<span class="bold signature">NILA PETRONILA R. OLIVARIO</span><br>
				<small>Assistant City Treasurer</small>
			</p>
		</div>
		<div class="col-xs-4">
			<p class="m-t-10 m-b-20">
				CERTIFIED: Completeness &amp; proprierty of supporting document
				previous cash advance liquidated/existence of fund held in trust
			</p>
			<p class="text-center">
				<span class="bold signature">ASTER P. MARASIGAN</span><br>
				<small>Asst. City Accountant-OIC</small>
			</p>
		</div>
		<div class="col-xs-3">
			<p class="m-b-20">APPROVED FOR PAYMENT:</p>
			<p class="text-center">
				<span class="bold signature">BEVERLEY ROSE A. DIMACHUA</span><br>
				<small>Administrative Officer V</small>
			</p>
			<p class="m-b-20">
				CERTIFIED: Each employee whose name appears above has been paid the
				amount indicated opposite his/her name.
			</p>
			<div class="row">
				<div class="col-xs-6">
					<p class="text-center">
						<span class="bold signature">Disbursing Officer</span>
					</p>
				</div>
				<div class="col-xs-6">
					<p class="text-center">
						<span class="bold signature">&nbsp;&nbsp;&nbsp;&nbsp;Date&nbsp;&nbsp;&nbsp;&nbsp;</span>
					</p>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- /basic modal -->
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<script type="text/javascript">
	$(function(){

		$('#target-table').delegate('.action-delete', "click", function(){
			$("#btn-confirm-delete").attr({"href" : $(this).data('url')});
		});

		$('#date_range').daterangepicker({ 
	    	autoApply: true,
			timePicker: false,
			locale: {
				format: 'YYYY-MM-DD'
			}
		});

	    $("#subcategory").on('change', function(){
	    	var selected = $(this).val();
	    	if(selected == "scholarship") {
	    		
	    	} else {
	    		$("#school_id").val('').trigger("change");
	    	}
	    });

	    $("#target").submit(function(e){
	    	$(".err-msg").remove();
	    	$(".has-error").removeClass("has-error");
	    });

	});
</script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/loaders/progressbar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_loaders.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>
@stop