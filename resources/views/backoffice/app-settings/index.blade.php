@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-cog"></i> <span class="text-semibold">App Settings</span> - List of all the settings stored in this application.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.app_settings.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Create New</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">App Settings</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Record Data</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			Here are the list of <code>all settings</code> in this application. <strong>Manage each row by clicking the action button on the far right portion of the table.</strong>
		</div>

		<table class="table datatable-basic table-hover" id="target-table">
			<thead>
				<tr>
					<th class="text-center" width="5%">#</th>
					<th>Title</th>
					<th>Code</th>
					<th class="text-center">Type</th>
					<th class="text-center">Status</th>
					<th class="text-center">Last Modified</th>
					<th class="text-center" width="7%"></th>
				</tr>
			</thead>
			<tbody>
			@foreach($settings as $index => $setting)
				<tr>
					<td class="text-center">{{++$index}}</td>
					<td>{{$setting->title}}</td>
					<td>{{$setting->code}}</td>
					<td>{{$setting->type}}</td>
					<td class="text-center">{{$setting->status}}</td>
					<td class="text-center" title="{{$setting->updated_at}}">{{$setting->last_modified()}}</td>
					<td class="text-center">
						<div class="btn-group">
	                    	<button type="button" class="btn btn-primary btn-xs btn-raised dropdown-toggle" data-toggle="dropdown">action <span class="caret"></span></button>
	                    	<ul class="dropdown-menu dropdown-menu-right">
								<li><a href="{{route('backoffice.app_settings.edit',[$setting->id])}}"><i class="icon-pencil3"></i> View/Edit</a></li>
								<li><a href="#" class="action-delete" data-url="{{route('backoffice.app_settings.destroy',[$setting->id])}}" data-toggle="modal" data-target="#confirm-delete"><i class="icon-eraser3"></i> Delete</a></li>
							</ul>
						</div>
					</td>
				</tr>
			@endforeach
				
			</tbody>
		</table>
	</div>

	@include('backoffice._includes.footer')

</div>
<!-- /content area -->
@stop
@section('modals')
<!-- Basic modal -->
<div id="confirm-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-warning alert-styled-left text-default content-group">
	                <span class="text-semibold">Warning!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Deleting Record...</h6>
				<p>You are about to delete a record, this action can no longer be undone, are you sure you want to proceed?</p>

				<hr>

				<h6 class="text-semibold">What is this message?</h6>
				<p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				<a href="" type="button" class="btn btn-danger" id="btn-confirm-delete">Delete</a>
			</div>
		</div>
	</div>
</div>
<!-- /basic modal -->
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript">
	$(function(){

		$('#target-table').delegate('.action-delete', "click", function(){
			$("#btn-confirm-delete").attr({"href" : $(this).data('url')});
		});
	});
</script>
@stop