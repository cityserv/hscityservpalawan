@extends("backoffice._template.queue")

@section('content')
<div class="content">
	<div class="row">
		<div class="col-md-6">
			<video styles="width: 100%; height: 300px;" autoplay="autoplay">
			  <source src="{{asset('backoffice/videos/1.mp4')}}" type="video/mp4">
				Your browser does not support the video tag.
			</video>
		</div>
		<div class="col-md-6">
			<h4 class="content-group text-semibold">Now Serving!</h4>
			<div class="row">
				<div class="col-md-4">
					<div class="panel panel-body border-top-success text-center">
						<h6 class="no-margin text-semibold">PRIORITY LANE</h6>
						<h1 class="well bg-success-400">S-0001</h1>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-body border-top-success text-center">
						<h6 class="no-margin text-semibold">HEALTH SERVICE</h6>
						<h1 class="well bg-success-400">H-0003</h1>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-body border-top-success text-center">
						<h6 class="no-margin text-semibold">SCHOLARSHIP PROG.</h6>
						<h1 class="well bg-success-400">E-0120</h1>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-body border-top-success text-center">
						<h6 class="no-margin text-semibold">SOCIAL SERVICE</h6>
						<h1 class="well bg-success-400">S-0002</h1>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-body border-top-success text-center">
						<h6 class="no-margin text-semibold">LEGAL SERVICE</h6>
						<h1 class="well bg-success-400">L-0030</h1>
					</div>
					<div class="clearfix"></div>
				</div>
				<div class="col-md-4">
					<div class="panel panel-body border-top-success text-center">
						<h6 class="no-margin text-semibold">ADMINISTRATIVE</h6>
						<h1 class="well bg-success-400">A-0100</h1>
					</div>
					<div class="clearfix"></div>
				</div>

			</div>
		</div>
	</div>
</div>
@stop

@section('page-scripts')
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/swfobject.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery-1.10.2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery.dotdotdot-1.5.1.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery.address.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery.mousewheel.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery.jscrollpane.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery.easing.1.3.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery.apPlaylistManager.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery.vg.settings_noPlaylist.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery.func.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery.vg.func.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/videoplayer/js/jquery.videoGallery.min.js')}}"></script>
@stop