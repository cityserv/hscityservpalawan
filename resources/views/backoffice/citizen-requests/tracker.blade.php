@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-certificate"></i> <span class="text-semibold">Mayor's Action Center</span> - Update a MAC request task.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.citizen_requests.index')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack text-primary"></i><span>All Data</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li><a href="{{route('backoffice.citizen_requests.index')}}"> Mayor's Action Center</a></li>
			<li class="active">Tracker</li>
		</ul>
	</div>
</div>
<!-- /page header -->


<!-- Content area -->
<div class="content">
	<form id="target" class="form-horizontal" action="" method="POST" enctype="multipart/form-data">
		<input type="hidden" name="_token" value="{{csrf_token()}}">
		<div class="panel panel-flat">
			<div class="panel-heading">
				<h5 class="panel-title">MAC Request Details</h5>
				<div class="heading-elements">
					<ul class="icons-list">
	            		<li><a data-action="collapse"></a></li>
	            		<!-- <li><a data-action="reload"></a></li> -->
	            		<!-- <li><a data-action="close"></a></li> -->
	            	</ul>
	        	</div>
			</div>

			<div class="panel-body">
				
				<p class="content-group-lg">Below are the general information for this task.</p>

				<div class="form-group">
					<label for="task" class="control-label col-lg-2 text-right">Request Code</label>
					<div class="col-lg-9">
						<h6 class="pt-10 text-semibold no-margin">{{$request->code}}</h6>
					</div>
				</div>

				<div class="form-group">
					<label for="task" class="control-label col-lg-2 text-right">Tracking Number</label>
					<div class="col-lg-9">
						<h6 class="pt-10 text-semibold no-margin">{{$request->tracking_number}}</h6>
					</div>
				</div>
				
				<div class="form-group {{$errors->first('task') ? 'has-error' : NULL}}">
					<label for="task" class="control-label col-lg-2 text-right">Task</label>
					<div class="col-lg-9">
						<h6 class="pt-10 text-semibold no-margin" id="task">{{$tracker->process_title}}</h6>
						{{-- <input type="hidden" name="task" value="{{old('task',$tracker->process_title)}}"> --}}
						@if($errors->first('task'))
						<span class="help-block">{{$errors->first('task')}}</span>
						@endif
					</div>
				</div>
				
				@if($auth->type == "super_user")
				<div class="form-group {{$errors->first('from') ? 'has-error' : NULL}}">
					<label for="from" class="control-label col-lg-2 text-right">From <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input type="text" name="from" class="form-control daterange-single" value="{{old('from', $tracker->from ? : Carbon::now())}}">
						@if($errors->first('from'))
						<span class="help-block">{{$errors->first('from')}}</span>
						@endif
					</div>
				</div>

				<div class="form-group {{$errors->first('to') ? 'has-error' : NULL}}">
					<label for="to" class="control-label col-lg-2 text-right">To <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input type="text" name="to" class="form-control daterange-single" value="{{old('to', $tracker->from ? Carbon::parse($tracker->from)->addHour() : Carbon::now()->addHour())}}">
						@if($errors->first('to'))
						<span class="help-block">{{$errors->first('to')}}</span>
						@endif
					</div>
				</div>
				@endif

				{{-- <div class="form-group {{$errors->first('status') ? 'has-error' : NULL}}">
					<label for="status" class="control-label col-lg-2 text-right">Status <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						{!!Form::select("status", $statuses, old('status'), ['id' => "status", 'class' => "select col-xs-12 col-sm-12 col-md-12 col-lg-5"])!!}
						@if($errors->first('status'))
						<span class="help-block">{{$errors->first('status')}}</span>
						@endif
					</div>
				</div> --}}

				<div class="form-group pl-10">
					<div class="checkbox col-lg-offset-2 col-lg-5">
						<label>
							<input type="checkbox" id="status" name="status" class="styled" value="done" {{old('status') == "done" ? 'checked' : NULL}}>
							Done
							<span class="help-block">Is this task done? If <code>unchecked</code>, please provide remarks below.</span>
						</label>
					</div>
				</div>

				<div class="form-group {{$errors->first('remarks') ? 'has-error' : NULL}}">
					<label for="remarks" class="control-label col-lg-2 text-right">Remarks</label>
					<div class="col-lg-9">
						<textarea class="form-control" name="remarks" id="remarks" placeholder="Write something.." rows="7">{{old('remarks')}}</textarea>
						@if($errors->first('remarks'))
						<span class="help-block">{{$errors->first('remarks')}}</span>
						@endif
					</div>
				</div>

				@if($is_last_process)
				<div class="form-group {{$errors->first('release_amount') ? 'has-error' : NULL}}">
					<label for="release_amount" class="control-label col-lg-2 text-right">Release Amount <span class="text-danger"> *</span></label>
					<div class="col-lg-9">
						<input type="text" name="release_amount" class="form-control" value="{{old('release_amount', $tracker->release_amount ? : '0.0')}}">
						@if($errors->first('release_amount'))
						<span class="help-block">{{$errors->first('release_amount')}}</span>
						@endif
					</div>
				</div>
				<input type="hidden" name="is_last_process" value="true">
				@endif
				
			</div>
		</div>

		<div class="content-group">
			<div class="text-left">
				<button id="save" type="submit" data-loading-text="<i class='icon-spinner2 spinner position-left'></i> Saving ..." class="btn btn-primary btn-raised btn-lg btn-loading">Save</button>
				&nbsp;
				<a type="button" class="btn btn-default btn-raised btn-lg" href="{{route('backoffice.citizen_requests.edit',[$request->id])}}">Cancel</a>
			</div>
		</div>
	</form>
	@include('backoffice._includes.footer')
</div>
<!-- /content area -->
@stop
@section('modals')
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/spin.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/buttons/ladda.min.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/styling/uniform.min.js')}}"></script>

<!-- Select2 -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/select2.js')}}"></script>

<script type="text/javascript" src="{{asset('backoffice/js/pages/form_inputs.js')}}"></script>

<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<script type="text/javascript">
	$(function(){

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.btn-loading').click(function () {
	        var btn = $(this);
	        btn.button('loading');
	    });

	    $(".styled, .multiselect-container input").uniform({
	        radioClass: 'choice'
	    });

	    $('.select').each(function(){
	    	$id = "#" + $(this).attr('id') + " option:first";
	    	$($id).prop('disabled',1);
	    });

	    $('.daterange-single').daterangepicker({ 
			singleDatePicker: true,
			timePicker: true,
			locale: {
				format: 'YYYY-MM-DD HH:mm'
			}
		});

		$('#status').on('change', function(e){

			var task = $('#task').text();

			if( $(this).prop("checked") ) {
				$("#remarks").val(task + " done.");
				$("#remarks").prop("readonly", true);
			} else {
				$("#remarks").val("");
				$("#remarks").prop("readonly", false);
			}
		});

	});
</script>
@stop