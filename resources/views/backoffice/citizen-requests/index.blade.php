@extends('backoffice._template.main')
@section('content')
<!-- Page header -->
<div class="page-header page-header-default">
	<div class="page-header-content">
		<div class="page-title">
			<h4><i class="icon-certificate"></i> <span class="text-semibold">Mayor's Action Center</span> - List of all the MAC requests stored in this application.</h4>
		</div>
		<div class="heading-elements">
			<div class="heading-btn-group">
				<a href="{{route('backoffice.citizen_requests.create')}}" class="btn btn-link btn-float text-size-small has-text"><i class="icon-stack-plus text-primary"></i><span>Create New</span></a>
			</div>
		</div>
	</div>

	<div class="breadcrumb-line">
		<ul class="breadcrumb">
			<li><a href="{{route('backoffice.dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
			<li class="active">Mayor's Action Center</li>
		</ul>
	</div>
</div>
<!-- /page header -->

<!-- Content area -->
<div class="content">

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Filter</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            	</ul>
        	</div>
		</div>
		<div class="panel-body">
			<form class="form-horizontal">
				<div class="form-group {{$errors->first('date_range') ? 'has-error' : NULL}}">
					<label for="date_range" class="control-label col-lg-2 text-right">Date Range</label>
					<div class="col-lg-8">
						<input type="text" id="date_range" name="date_range" class="form-control daterange-basic" placeholder="YYYY-MM-DD" value="{{old('date_range', $date_range)}}">
						@if($errors->first('date_range'))
						<span class="help-block">{{$errors->first('date_range')}}</span>
						@endif
					</div>
					<div class="col-lg-1">
						<button id="save" type="submit" class="btn btn-primary btn-raised btn-xs">Apply</button>
					</div>
				</div>
			</form>
		</div> 
	</div>

	<div class="panel panel-flat">
		<div class="panel-heading">
			<h5 class="panel-title">Record Data</h5>
			<div class="heading-elements">
				<ul class="icons-list">
            		<li><a data-action="collapse"></a></li>
            		<!-- <li><a data-action="reload"></a></li> -->
            		<!-- <li><a data-action="close"></a></li> -->
            	</ul>
        	</div>
		</div>

		<div class="panel-body">
			Here are the list of <code>all MAC requests</code> in this application. <strong>Manage each row by clicking the action button on the far right portion of the table.</strong>
		</div>

		<div class="table-responsive">
			<table class="table datatable-basic table-hover" id="target-table">
				<thead>
					<tr>
						<th class="text-center">#</th>
						<th width="15%">Tracking #</th>
						<th width="20%">Request Info</th>
						<th class="text-center">Status</th>
						<th  width="15%" class="text-center">Date of Request</th>
						<th  width="15%" class="text-center">Appointment Date</th>
						<th class="text-center" width="7%"></th>
					</tr>
				</thead>
				<tbody>
				@foreach($requests as $index => $request)
					<tr>
						<td class="text-center">{{++$index}} </td>
						<td><strong>{{Str::upper($request->tracking_number?:"---")}}</strong></td>
						<td class="no-margin" >
							<p class="text-bold no-margin" data-popup="tooltip" title="{{$request->title}}" data-placement="bottom">{{Str::limit($request->title,25)}}</p>
							<div><small>by {{$request->name ? : "---"}}</small></div>
						</td>
						<td class="text-center">
							<small class="text-semibold">{{($request->tracker ? $request->tracker->num_completed : 0) ." / " . ($request->tracker ? $request->tracker->num_process : 0)}} Completed</small>

							<div class="progress">
								<div class="progress-bar  {{$request->tracker->percentage != "100" ? 'progress-bar-striped active' : NULL}} {{Helper::progress_color($request->tracker ? $request->tracker->percentage : 0)}}" style="width: {{$request->tracker ? $request->tracker->percentage : 0}}%">
									
								</div>
							</div>
						</td>
						<td class="text-center" title="{{$request->created_at}}">{{$request->date_format($request->created_at,'Y-m-d H:i')}}</td>
						<td class="text-center" title="{{$request->appointment_date}}">{{$request->date_format($request->tracker->appointment_schedule,'Y-m-d H:i')}}</td>
						<td class="text-center">
							<div class="btn-group">
		                    	<button type="button" class="btn btn-primary btn-xs btn-raised dropdown-toggle" data-toggle="dropdown">action <span class="caret"></span></button>
		                    	<ul class="dropdown-menu dropdown-menu-right">
									<li><a href="{{route('backoffice.citizen_requests.transaction',[$request->id])}}"><i class="icon-eye"></i> View Transaction</a></li>
									<li class="divider"></li>
									<li><a href="{{route('backoffice.citizen_requests.edit',[$request->id])}}"><i class="icon-quill2"></i> Edit Request</a></li>
									<li><a href="#" class="action-delete" data-url="{{route('backoffice.citizen_requests.destroy',[$request->id])}}" data-toggle="modal" data-target="#confirm-delete"><i class="icon-eraser3"></i> Delete</a></li>
								</ul>
							</div>
						</td>
					</tr>
				@endforeach	
				</tbody>
			</table>
		</div>
	</div>

	@include('backoffice._includes.footer')

</div>
<!-- /content area -->
@stop
@section('modals')
<!-- Basic modal -->
<div id="confirm-delete" class="modal fade">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h5 class="modal-title">Confirm your action</h5>
			</div>

			<div class="modal-body">
				
				<div class="alert alert-warning alert-styled-left text-default content-group">
	                <span class="text-semibold">Warning!</span> This action can not be undone.
	                <button type="button" class="close" data-dismiss="alert">×</button>
	            </div>

				<h6 class="text-semibold">Deleting Record...</h6>
				<p>You are about to delete a record, this action can no longer be undone, are you sure you want to proceed?</p>

				<hr>

				<h6 class="text-semibold">What is this message?</h6>
				<p>This dialog appears everytime when the chosen action could hardly affect the system. Usually, it occurs when the system is issued a delete command.</p>
			</div>

			<div class="modal-footer">
				<button type="button" class="btn btn-link" data-dismiss="modal">Close</button>
				<a href="" type="button" class="btn btn-danger" id="btn-confirm-delete">Delete</a>
			</div>
		</div>
	</div>
</div>
<!-- /basic modal -->
@stop
@section('page-styles')
@stop
@section('page-scripts')
@include('backoffice._includes.page-jgrowl')
<script type="text/javascript" src="{{asset('backoffice/js/plugins/tables/datatables/datatables.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/custom/datatable.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/forms/selects/select2.min.js')}}"></script>
<!-- Daterange Picker -->
<script type="text/javascript" src="{{asset('backoffice/js/plugins/ui/moment/moment.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/plugins/pickers/daterangepicker.js')}}"></script>

<script type="text/javascript">
	$(function(){

		$('#target-table').delegate('.action-delete', "click", function(){
			$("#btn-confirm-delete").attr({"href" : $(this).data('url')});
		});

		 $('#date_range').daterangepicker({ 
	    	autoApply: true,
			timePicker: false,
			locale: {
				format: 'YYYY-MM-DD'
			}
		});
	});
</script>

<script type="text/javascript" src="{{asset('backoffice/js/plugins/loaders/progressbar.min.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_loaders.js')}}"></script>
<script type="text/javascript" src="{{asset('backoffice/js/pages/components_popups.js')}}"></script>
@stop